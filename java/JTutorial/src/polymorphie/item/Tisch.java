package polymorphie.item;

public class Tisch extends Item {
	
	private String material;
	
	public Tisch(String name, double preis){
		super(name,preis);
		material = "";
	}

	@Override
	public void setMaterial(String material) {
		this.material = material;
	}

	@Override
	public String getMaterial() {
		return material;
	}
	
	public void t(){}

}
