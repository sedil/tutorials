package polymorphie.item;

public class Stuhl extends Item {
	
	private String material;
	
	public Stuhl(String name, double preis){
		super(name,preis);
		material = "";
	}

	@Override
	public void setMaterial(String material) {
		this.material = material;
	}

	@Override
	public String getMaterial() {
		return material;
	}

}
