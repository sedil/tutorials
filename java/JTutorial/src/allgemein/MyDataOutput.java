package allgemein;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;

public class MyDataOutput extends Thread {
	
	private final int PORT = 9998;
	private final String DATA = "/home/sebastian/Schreibtisch/test";
	
	public void stopDataServer(){
		interrupt();
	}
	
	@Override
	public void run() {
		FileInputStream fis = null;
		BufferedInputStream bis = null;
		ObjectOutputStream os = null;
		ServerSocket servsock = null;
		Socket sock = null;
		try {
			servsock = new ServerSocket(PORT);
			while (true) {
				System.out.println("Warte auf Verbindung...");
				try {
					sock = servsock.accept();
					System.out.println("Verbunden : " + sock);
					// send file
					File myFile = new File(DATA);
					byte [] mybytearray  = new byte [(int)myFile.length()];
					fis = new FileInputStream(myFile);
					bis = new BufferedInputStream(fis);
					bis.read(mybytearray,0,mybytearray.length);
					os = new ObjectOutputStream(sock.getOutputStream());
					System.out.println("Sending " + DATA + "(" + mybytearray.length + " bytes)");
					os.write(mybytearray,0,mybytearray.length);
					os.flush();
					System.out.println("Done.");
				} catch ( Exception e ){
					e.printStackTrace();
				} finally {
					if (bis != null) {
						try {
							bis.close();
						} catch (IOException e) {
							e.printStackTrace();
						}
					}
					if (os != null) {
						try {
							os.close();
						} catch (IOException e) {
							e.printStackTrace();
						}
					}
					if (sock!=null) {
						try {
							sock.close();
						} catch (IOException e) {
							e.printStackTrace();
						}
					}
				}
			}
		} catch ( Exception e) {
			e.printStackTrace();
		} finally {
			if (servsock != null) {
				try {
					servsock.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}
	
	public static void main (String [] args ) throws Exception {
		new MyDataOutput().start();
	}
}

