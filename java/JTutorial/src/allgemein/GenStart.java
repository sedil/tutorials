package allgemein;

import java.util.ArrayList;
import java.util.List;

public class GenStart {
	public static void main(String[] args){
		new GenStart();
	}
	
	GenStart(){
		/* Mit Generics gibt es die Moeglichkeit, Datentypen zu
		 * parametrisieren. Erst bei der Implementierung muss
		 * man sich fuer ein Datentyp entscheiden
		 */
		GenPunkt<Integer> a = new GenPunkt<Integer>(3,5);
		GenPunkt<Double> b = new GenPunkt<Double>(3.1,5.8);
		//GenPunkt<Boolean> c = new GenPunkt<Boolean>(false,true);
		GenPunkt<Float> d = new GenPunkt<Float>(1.5f, 3.4f);
		
		System.out.println(a.pointToString());
		System.out.println(b.pointToString());
		//System.out.println(c.pointToString());
		System.out.println(d.pointToString());
		
		// Generische Funktion aufrufen
		a.printGenType(new Integer(3));
		a.printGenType(new Long(35));
		
		// Ein Beispiel mit Wildcard
		List<Number> nums = new ArrayList<Number>();
		nums.add(new Integer(5));
		nums.add(new GenPunkt<Integer>(3,5).intValue());
		nums.add(new Double(1.4));
		nums.add(new Float(5.6f));
		double s = sumOfNumbers(nums);
		System.out.println("Summe : "+s);
	}
	
	/* Der uebergebene Datentyp muss in diesen Fall Number als
	 * Superklasse haben. Wildcard '?' nimmt man dann, wenn die
	 * eigene Klasse nicht generisch ist
	 */
	private double sumOfNumbers(List<? extends Number> numbers){
		double sum = 0.0;
		for(Number n : numbers){
			sum += n.doubleValue();
		}
		return sum;
	}
}
