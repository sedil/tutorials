package assoziation;

class Ost {
	
	private Zentrum zentrum;
	
	Ost(){}
	
	/* Uebergebe die Referenz zu Zentrum */
	void verbindeMitZentrum(Zentrum zentrum){
		this.zentrum = zentrum;
	}
	
	void drucke(){
		System.out.println("OST : 1 zu 1 bidirektional");
		zentrum.druckeOstZentrum();
	}

}
