package lambda;

/** for writing user functions to use lambda expressions */
@FunctionalInterface
public interface CalculatorFunction<X,Y,R> {

    public R calculate(X arg1, Y arg2);
    
}
