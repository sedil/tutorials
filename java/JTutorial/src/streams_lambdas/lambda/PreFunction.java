package lambda;

import java.util.List;

public class PreFunction {

    public PreFunction() { }

    /** lambda functions can also be an argument **/
    public <X,Y,R> R calculateNumbers(X arg1, Y arg2, CalculatorFunction<X,Y,R> func){
        return func.calculate(arg1, arg2);
    }

    public <X,R> R power(X arg1, Integer arg2, CalculatorFunction<X,Integer,R> func){
        return func.calculate(arg1, arg2);
    }

    public List<String> extractWords(String content, WordExtractor func){
        return func.extractWords(content);
    }
    
}
