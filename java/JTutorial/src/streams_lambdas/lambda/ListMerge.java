package lambda;

import java.util.List;
import java.util.ArrayList;

@FunctionalInterface
public interface ListMerge<T> {

    public List<T> combine(List<T> one, List<T> two);

    default List<Integer> scalar(List<Integer> list, Integer scalar){
        List<Integer> result = new ArrayList<Integer>();
        for(Integer elem : list){
            result.add(elem*scalar);
        }
        return result;
    }
    
}
