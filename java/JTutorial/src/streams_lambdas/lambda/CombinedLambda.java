package lambda;

import java.util.function.BiFunction;
import java.util.function.Function;

public class CombinedLambda {

    public CombinedLambda(){}

    public void testCombinedLambda(){
        // 1. add two integers and get the result
        BiFunction<Integer, Integer, Integer> add = (x,y) -> {return x+y;};
        // 2. multiply with 2
        Function<Integer,Integer> scalar = x -> {return x*2;};
        // 3. examine if the result from add is zero
        Function<Integer, Boolean> isZero = x -> {return x == 0 ? true : false;};

        // first execute apply, then execute andThen from left to right. The rightest andThen is the result
        var test = add.andThen(scalar).andThen(scalar).andThen(isZero).apply(3, 7);
        System.out.println(test);
    }
    
}
