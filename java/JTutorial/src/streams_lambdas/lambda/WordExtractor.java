package lambda;

import java.util.List;

/** for writing user functions to use lambda expressions */
@FunctionalInterface
public interface WordExtractor {
    
    public List<String> extractWords(String content);
}
