import stream.*;
import lambda.WordExtractor;
import lambda.PreFunction;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

public class Main {

    public Main(){
        FileHandler fh = new FileHandler();
        String content = fh.readFile("path-to-file");
        WordExtractor lambda = (text) -> {return Arrays.asList(content.split(" "));};
        PreFunction pf = new PreFunction();
        List<String> wordlist = pf.extractWords(content, lambda);
        StreamProcessing stream = new StreamProcessing();
        Map<String, Integer> wordmap = stream.normalize(wordlist);
        stream.printWordmap(wordmap);
        fh.saveWordlist("path-to-save", wordmap);
    }
    public static void main(String[] args){
        new Main();
    }
    
}
