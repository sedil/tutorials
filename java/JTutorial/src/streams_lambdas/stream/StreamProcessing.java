package stream;

import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.List;
import java.util.Arrays;

public class StreamProcessing {

    public StreamProcessing() {}

    public Map<String, Integer> normalize(List<String> words){
        /** var is like auto in c++ */
        var wordmap = new HashMap<String, Integer>();
        words.stream()
        .filter(elem -> elem.length() >= 5)
        .map(elem -> elem.toLowerCase())
        .forEach(elem -> {
            if (wordmap.containsKey(elem) ){
                Integer count = wordmap.get(elem);
                wordmap.put(elem, count + 1);
            } else {
                wordmap.put(elem, 1);
            }
        });
        return wordmap;
    }

    public void printWordmap(Map<String, Integer> wordmap){
        for(var entry : wordmap.entrySet()){
            System.out.println(entry.getKey()+" "+entry.getValue());
        }
    }

    public void createStream(){
        List<Integer> numbers = Arrays.asList(-2,-1,0,1,2,3,4,5);

        /** long way **/
        //Stream<Integer> stream = numbers.stream();
        //Stream<Integer> filter1 = stream.filter(elem -> elem >= 0);
        //Stream<Integer> filter2 = filter1.filter(elem -> elem % 2 == 0);
        //List<Integer> result = filter2.collect(Collectors.toList());
        //result.forEach(System.out::println);

        numbers.stream()
        .filter(elem -> elem >= 0 && elem % 2 == 0)
        .collect(Collectors.toList())
        .forEach(System.out::println);
    }

    public int sumUpListNumbers(List<Integer> numbers){
        return numbers.stream().reduce(0, (x,y) -> x + y);
    }
}