package stream;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.util.Map;

public class FileHandler {

    public FileHandler() { }

    public String readFile(String path){
        if ( path == null || path.isEmpty() ){
            return new String();
        }

        File file = new File(path);
        StringBuilder sb = new StringBuilder();
        try {
            BufferedReader br = new BufferedReader(new FileReader(file));
            String line = null;
            while( (line = br.readLine()) != null) {
                sb.append(line +"\n");
            }
            br.close();
            return sb.toString();
        } catch ( IOException err ){
            System.out.println(err);
            return new String();
        }
    }

    public void saveWordlist(String path, Map<String, Integer> wordmap){
        File file = new File(path);
        try {
            BufferedWriter bw = new BufferedWriter(new FileWriter(file, false));
            for(var elem : wordmap.entrySet()){
                bw.write(elem.getKey()+" "+elem.getValue()+"\n");
            }
            bw.flush();
            bw.close();
        } catch ( IOException err ){
            System.out.println(err);
        }
    }
    
}
