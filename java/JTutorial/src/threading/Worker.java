package threading;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/* Beispiel mit Synchronisationsbloecken */
public class Worker {
	
	private Random random;
	private Object lockOne, lockTwo;
	private List<Integer> listOne, listTwo;
	
	public Worker(){
		random = new Random();
		lockOne = new Object();
		lockTwo = new Object();
		listOne = new ArrayList<Integer>();
		listTwo = new ArrayList<Integer>();
		
		demonstrate();
	}
	
	private void demonstrate(){
		System.out.println("Start...");
		long start = System.currentTimeMillis();
		createThreads();
		long ende = System.currentTimeMillis();
		long diff = ende - start;
		System.out.println("Dauer : "+diff);
		System.out.println("Liste 1 : "+listOne.size()+", Liste 2 : "+listTwo.size());
	}
	
	private void createThreads(){
		Thread one = new Thread(new Runnable(){
			
			@Override
			public void run(){
				process();
			}
		});
		
		Thread two = new Thread(new Runnable(){
			
			@Override
			public void run(){
				process();
			}
		});

		one.start();
		two.start();
		
		try {
			one.join();
			two.join();
		} catch (InterruptedException ie){
			System.err.println(ie.toString());
		}
	}
	
	/* Mehrere Threads koennen zwar in die Funktion, aber nur ein
	 * Thread kann in den synchronisationsblock, andere Threads
	 * mÃ¼ssen warten */
	private void stageOne(){
		synchronized ( lockOne ){
			try {
				Thread.sleep(1);
			} catch (InterruptedException ie){
				System.err.println(ie.toString());
			}
			listOne.add(random.nextInt(100));
		}
	}
	
	private void stageTwo(){
		synchronized ( lockTwo ){
			try {
				Thread.sleep(1);
			} catch (InterruptedException ie){
				System.err.println(ie.toString());
			}
			listTwo.add(random.nextInt(100));
		}
	}
	
	private void process(){
		for(int i = 0; i < 1000; i++){
			stageOne();
			stageTwo();
		}
	}
}
