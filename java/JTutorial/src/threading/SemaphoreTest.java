package threading;

import java.util.concurrent.Semaphore;

/* Nur n Threads (hier 1 bei Semaphore) darf auf ein geschuetzen Bereich zugreifen */
public class SemaphoreTest {
	
	private Semaphore semaphore;
	
	public SemaphoreTest(){
		semaphore = new Semaphore(1);
	}

    public void protectedArea() {
        try {
        	semaphore.acquire();

            //mutual exclusive region
            System.out.println(Thread.currentThread().getName() + " befindet sich im geschuetzten Bereich");
            Thread.sleep(1000);

        } catch (InterruptedException ie) {
            ie.printStackTrace();
        } finally {
        	semaphore.release();
            System.out.println(Thread.currentThread().getName() + " ist nicht mehr im geschuetzten Bereich");
        }
    } 
}

/* In die main kopieren, zum testen
final SemaphoreTest st = new SemaphoreTest();

new Thread(){
    @Override
    public void run(){
      st.protectedArea();
    }
}.start();

new Thread(){
    @Override
    public void run(){
    	st.protectedArea();
    }
}.start();
*/
