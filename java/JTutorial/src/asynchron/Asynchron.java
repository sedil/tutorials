import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.function.Supplier;

public class Asynchron {

    public Asynchron(){}

    /** dummy function for runAsync **/
    private void printdelay(int delay){
        try {
            TimeUnit.SECONDS.sleep(delay);
            System.out.println("dummy");
        } catch (InterruptedException e){
            e.printStackTrace();
        }
    }

    /** dummy function for supplyAsync **/
    private int countdelay(int x, int y){
        try {
            TimeUnit.SECONDS.sleep(3);
            return x+y;
        } catch (InterruptedException e){
            e.printStackTrace();
            return -1;
        }
    }

    public void runAsyncTest(){
        // create a runnableobject
        ExecutorService worker = Executors.newCachedThreadPool(); // with workerthreads
        Runnable run = () -> {
            printdelay(2);
            System.out.println("Inside Runnable : " + Thread.currentThread().getName());
        };

        // create completablefuture
        CompletableFuture<Void> future = CompletableFuture.runAsync(run,worker);
        System.out.println("Inside main : " + Thread.currentThread().getName());
        future.join();
    }

    public void supplyAsyncTest(){
        Supplier<Integer> result = () -> {
            int calculation = countdelay(3,7);
            System.out.println("Inside Supplier : " + Thread.currentThread().getName());
            return calculation;
        };

        CompletableFuture<Integer> future = CompletableFuture.supplyAsync(result);
        System.out.println("Inside main : " + Thread.currentThread().getName());
        int fromFuture = future.join();
        System.out.println(fromFuture);
    }

    public void callbackAsyncTest(){

        // callbackfunctions can be chained with the helpm of functional interfaces and
        // predefined functions from the completable futures like thenApply, thenAccept, theRun ...
        CompletableFuture<Void> future = CompletableFuture.supplyAsync(
            () -> {
                return countdelay(2,3);
            }
        ).thenApply(
            (addresult) -> {
                return addresult * 3;
            }
        ).thenAccept(
            (multresult) -> {
                System.out.println("Final result is : " + multresult);
            }
        ).thenRun(
            () -> {
                System.out.println("Future with Callback complete");
            }
        );

        System.out.println("Inside main : " + Thread.currentThread().getName());
        future.join();
    }

    /** dummy function for dependent futures 1 **/
    private CompletableFuture<String> getForename(){
        return CompletableFuture.supplyAsync(
            () -> {
                printdelay(3);
                return new String("Heinrich");
            }
        );
    }

    /** dummy function for dependent futures 2 **/
    private CompletableFuture<String> getLastname(String surname){
        return CompletableFuture.supplyAsync(
            () -> {
                printdelay(1);
                return new String(surname + " Skalitz");
            }
        ); 
    }

    // lastname is dependend on forename
    public void dependentFutureTest(){
        CompletableFuture<String> future = getForename().thenCompose(
            (forename) -> {
                return getLastname(forename);
            }
        );

        System.out.println("Inside main : " + Thread.currentThread().getName());
        String fullname = future.join();
        System.out.println("Name : " + fullname);
    }

    // in simple words : add two independent futures
    public void independentFutureTest(){
        CompletableFuture<String> future = getForename().thenCombine(
            (getLastname("Hermann")),(x,y) -> {
                return x+y;
            }
        );

        System.out.println("Inside main : " + Thread.currentThread().getName());
        String fullname = future.join();
        System.out.println("Name : " + fullname);

    }

    public void allOfTest(){
        CompletableFuture<Integer> future1 = CompletableFuture.supplyAsync(
            () -> {
                return 5;
            }
        );

        CompletableFuture<String> future2 = CompletableFuture.supplyAsync(
            () -> {
                return "Word";
            }
        );

        CompletableFuture<Integer> future3 = CompletableFuture.supplyAsync(
            () -> {
                return 3;
            }
        );

        // wait until all futures are complete
        CompletableFuture<Void> waitAllFuture = CompletableFuture.allOf(future1,future2,future3).thenAccept(
            f -> {
                System.out.println(future1.join());
                System.out.println(future2.join());
                System.out.println(future3.join());
            }
        );
        System.out.println("Inside main : " + Thread.currentThread().getName());
        waitAllFuture.join();

        // wait until one of the futures from the list is complete and get the result of this future
        CompletableFuture<Object> waitAnyFuture = CompletableFuture.anyOf(future1,future2,future3);
        Object fastesResult = waitAnyFuture.join();
        System.out.println(fastesResult);
    }

    public void exceptionFutureTest(){

        // exceptionally expect exceptionmessage as argument
        boolean errorflag = false;
        CompletableFuture<Integer> future1 = CompletableFuture.supplyAsync(
            () -> {
                if(errorflag) throw new NullPointerException("Nullpointer");
                return 1;
            }
        ).exceptionally(
            (e) -> {
                System.out.println(e.getMessage());
                return -1;
            });

        System.out.println(future1.join());

        // handle expect the possibly result and the possibly exceptionmessage
        CompletableFuture<Integer> future2 = CompletableFuture.supplyAsync(
            () -> {
                if(errorflag) throw new NullPointerException("Again a Nullpointer");
                return 1;
            }
        ).handle(
            (s,e) -> {
                if(e != null){
                    System.out.println(e.getMessage());
                    return -1;
                }
                return s;
            }
        );

        System.out.println(future2.join());
    }

    public static void main(String[] args){
        Asynchron async = new Asynchron();
        //async.runAsyncTest();
        //async.supplyAsyncTest();
        //async.callbackAsyncTest();
        //async.independentFutureTest();
        //async.allOfTest();
        //async.exceptionFutureTest();
    }
    
}

/* runAsync : input Runnable, output Void
supplyAsync : input Supplier<T>, output T

Callback to CompletableFuture
thenRun : input Runnable, output Void
thenAccept : input Consumer<T>, output Void
thenApply : input Function<T,R>, output R

Composing dependent futures
thenCompose : input Function<T,R>, output R

Composing independent futures
thenCombine : input BiFunction<T,R,S>, output R,S

Multiple CompletableFutures
allOf : input Array of CompletableFuture, output Void
anyOf : input Array of CompletableFuture, output T

Exceptions
handle : input BiFunction<T,R,S>, output exception and result
exceptionally : input Function<T,R>, output exception only
*/
