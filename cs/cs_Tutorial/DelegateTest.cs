﻿using System;
using System.Linq;
using System.Collections.Generic;

namespace Tutorial {

    // (1) Einen Delegaten definieren entweder manuell mit delegate oder Func oder Action
    // (2) Einen Delegaten aufrufen, indem man eine Funktion schreibt dessen Struktur genauso aussieht wie der Delegate selbst.
    // (3) Konkrete implementierung eines Delegaten kann z.B. als Parameter einer Funktion übergeben werden

    public class DelegateTest {

        private List<int> numbersOne;
        private List<int> numbersTwo;

        public DelegateTest(){
            numbersOne = new List<int>();
            for(var n = 0; n < 10; n++){
                numbersOne.Add(n);
            }
            numbersTwo = new List<int>();
            for(var n = 0; n < 10; n++){
                numbersTwo.Add(2*n);
            }

            /* (3) */
            Process(IntToStringConverter,SumUpLists);
        }

        /* (1a) */
        private delegate string Convert(int number);

        /* (2a) */
        private string IntToStringConverter(int number){
            return number.ToString();
        }

        /* (2b) */
        private int SumUpLists(List<int> a, List<int> b){
            int sum = 0;
            foreach(var elem in a){
                sum += elem;
            }
            foreach(var elem in b){
                sum += elem;
            }
            return sum;
        }

        /* (2a) das Func zu dem eine passende Struktur geschrieben werden muss */
        private void Process(Convert convert, Func<List<int>,List<int>,int> listsum){
            int result = listsum(numbersOne,numbersTwo);
            Console.WriteLine(convert(result));
        }
    }
}
