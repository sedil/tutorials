using System;
using System.Collections.Generic;
using System.Threading;

namespace Tutorial {

public class SumArray{
	
	private int tasknumber;
	public int result { get; set;}

	public SumArray(int tasknumber){
		this.tasknumber = tasknumber;
	}
	
	/** vgl. Javas run-methode und object zum richtigen Typen casten **/
	public void countSum(object slice){
		int result = 0;
		foreach(int elem in (int[]) slice){
			result += elem;
		}
		Console.WriteLine(this.tasknumber + " : " + result);
	}
}

public class ThreadTest {

	public void countSumOfArray(){
		/** Die Aufgabe **/
		int[] numbers = new int[1000];
		int amountOfThreads = numbers.Length / 50;
		
		/** Fill Array with Numbers **/
		for(int i = 0; i < numbers.Length; i++){
			numbers[i] = i + 1;
		}
		
		/** Slice the Array **/
		int r = 0;
		for(int i = 0; i < amountOfThreads; i++){
			int[] partialArray = new int[50];
			for(int j = 0; j < amountOfThreads; j++){
				partialArray[j] = numbers[r * 50 + j];
			}
			r++;
			
			ThreadPool.QueueUserWorkItem(new WaitCallback(new SumArray(i).countSum), partialArray);
		}
	}
}
}
