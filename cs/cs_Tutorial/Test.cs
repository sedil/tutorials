using System;

namespace Tutorial {

    public class Test {
        
        private const string name = "Basis"; /* const = sofortige Zuweisung, kann im nachhinein nicht verändert werden */
	public readonly int x; /* readonly = Zuweisung kann auch im Konstruktor erfolgen und verändert werden */
        private int _amount;

        /** setter und getter in c# **/
        public int amount {
            set {
                if( value > 0 && value <= 100) {
                    _amount = value;	
                }
            }

            get {
                return _amount;	
            }
        }
        
        /** Konstruktor mit einem Default-Parameter **/
        public Test(int amount, short factor=1){
            this.amount = amount * factor;	
        }
        
        /** Funktionen **/
        public string getInfoFromInput(){
            string input = Console.ReadLine();
            return "Eingabe war :" + input;
        }
    }
} 
