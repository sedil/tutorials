using System;
using System.Threading;
using System.Threading.Tasks;
using System.Collections.Generic;

namespace Tutorial {

    public class AsyncTest {

        public AsyncTest() {}

        /* Eine async-Funktion muss eine await-Anweisung haben. Diese führt einen
           Task aus. */
        public async void StartTasksAsync(){
            List<int> numbers = new List<int>();
            for(int i = 0; i < 10; i++){
                numbers.Add(i+1);
            }
            int multi = await getMultiplier();
            numbers.ForEach(elem => elem *= multi);
            foreach(int elem in numbers){
                Console.WriteLine(elem);
            }
        }

        private Task<int> getMultiplier(){
            return Task<int>.Factory.StartNew(() => {
                int m = 0;
                for(int i = 0; i < 3; i++){
                    Console.WriteLine("Do work...");
                    m++;
                    Thread.Sleep(1000);
                }
                return m;
            });
        }
    }
}
