using System;

namespace Tutorial {

    /** Abstrakte Klasse **/
    public abstract class Tier {

        private int beine;
        
        public Tier(int beine=4) {
            this.beine = beine;
        }
        
        /** Wenn die Funktionalität einer Funktion in der Subklasse ueberschrieben werden soll **/
        protected virtual void printTier(){
            Console.WriteLine("Tier");
        }
        
        public abstract void funktion();
    }

    /** Ein Interface enthaelt nur Abstrakte Funktionen und Konstenten **/
    interface Aktion {

        void laufen(int speed);
    }

    /** Vererbung **/
    public class Katze : Tier, Aktion {
        
        private string name;
        
        public Katze(string name) : base(){
            this.name = name;
        }
        
        public void laufen(int speed){
            Console.WriteLine("Gehe " + speed + " schnell");	
        }
        
        protected override void printTier(){
            Console.WriteLine("Katze");
        }
        
        public override void funktion(){
            Console.WriteLine("Funktion in Katze");	
        }
        
        public string Name{
            get {
                return name;
            }
        }
    }

    /** sealed = Klasse kann nicht weitervererbt werden **/
    public sealed class Spinne : Tier, Aktion {
        
        private string name;
        
        public Spinne(string name) : base(8){
            this.name = name;
        }
        
        public void laufen(int speed){
            Console.WriteLine("Gehe " + speed + " schnell");	
        }
        
        protected override void printTier(){
            Console.WriteLine("Spinne");
        }
        
        public override void funktion(){
            Console.WriteLine("Funktion in Spinne");	
        }
        
        public string Name{
            get {
                return name;
            }
        }
    }
} 
