using System;
using System.Threading;

namespace Tutorial {

    // (1) Definiere ein delegate
    // (2) Definiere ein event, welches auf das delegate basiert
    // Mit EventHandler<> kann (1) und (2) zusammengefasst werden
    // (3) event ausführen
    // (4) event registrieren
    // (5) Optional: Benutzerdefinierte Events, um auf zusätzliche Daten zuzugreifen. Ansonsten nur EventArgs

    /* (5) */
    public class VideoEventArgs : EventArgs {

        public Video Video {get; set;}
    }

    public class Video {

        public string VideoTitle {get; set;}

        public Video(string title){
            VideoTitle = title;
        }

    }

    public class VideoEncoder {

        /* (1) */
        //public delegate void VideoEncodedEventHandler(object source, VideoEventArgs args);
        /* (2) */
        //public event VideoEncodedEventHandler VideoEncoded;

        // (1) + (2) EventHandler benötigt kein selbst geschriebenes delegate
        public event EventHandler<VideoEventArgs> VideoEncoded; // Mit Custom-class VideoEventArgs
        //public event EventHandler VideoEncoded; = Ohne Custom-class

        public void Encode(Video video){
            Console.WriteLine("Encode video");
            Thread.Sleep(2000);
            OnVideoEncoded(video); // Das Event wird aufgerufen
        }

        /* (3) */
        protected virtual void OnVideoEncoded(Video video){
            if ( VideoEncoded != null ){
                VideoEncoded(this, new VideoEventArgs() { Video = video} );
                // VideoEncodes(this, EventArgs.Empty); = Ohne Custom
            }
        }

    }

    public class MailService {

        /* Ein Event welches Registriert werden kann hat diese Struktur */
        public void OnVideoEncoded(object source, VideoEventArgs e){
            Console.WriteLine($"Sending an email... { e.Video.VideoTitle}");
        }

    }

    public class MessageService {

        public void OnVideoEncoded(object source, VideoEventArgs e){
            Console.WriteLine($"Sending a message... { e.Video.VideoTitle}");
        }

    }

    /*
    public class Start {

        public static void Main(string[] args){
            Video video = new Video("The Shining");
            VideoEncoder encoder = new VideoEncoder();

            // Beispiele für Eventausführungen
            MailService mail = new MailService();
            MessageService message = new MessageService();

            // (4) //
            encoder.VideoEncoded += mail.OnVideoEncoded;
            encoder.VideoEncoded += message.OnVideoEncoded;

            encoder.Encode(video);
        }
    }
    */

}