using System;

namespace Tutorial {

    public class Algorithmen {

        public int[] bubbleSort(int[] arr){
            for(int n = 0; n < arr.Length; n++){
                for(int m = 0; m < arr.Length - 1; m++){
                    if(arr[m] > arr[m+1]){
                        int temp = arr[m];
                        arr[m] = arr[m+1];
                        arr[m+1] = temp;
                    }
                }
            }
            return arr;
        }

        public int[] selectionSort(int[] arr){
            int maximum = Int32.MinValue;
            int idx = -1;
            for(int n = 0; n < arr.Length; n++){
                maximum = Int32.MinValue;
                for(int m = 0; m < arr.Length - n; m++){
                    if( arr[m] > maximum ){
                        maximum = arr[m];
                        idx = m;
                    }

                    if ( m == arr.Length - n - 1){
                        int temp = arr[arr.Length - n - 1];
                        arr[arr.Length - n - 1] = maximum;
                        arr[idx] = temp;
                    }
                }
            }
            return arr;
        }

    }

}