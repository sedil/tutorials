using System;
using System.Collections.Generic;

namespace Tutorial {

	// object initializer test struct //
    public class Person {

        public string? Firstname {get;init;}
        public string? Lastname {get;init;}

    }
	
	public class Basics {

		public Basics(){
			
			/** Bedingungen **/
			int x = 5;
			if (x < 5){
				Console.WriteLine(x + " is kleiner 5");
			} else {
				Console.WriteLine(x + " ist groessergleich 5");
			}
			
			/** Auswahl **/
			switch(x){
				case 1: {
					Console.WriteLine("Fall 1");
					break;
				}
				case 5: {
					Console.WriteLine("Fall 5");
					break;
				}
				default: {
					Console.WriteLine("Rest");
					break;
				}
			}
			
			int[] feld = new int[5];
			
			/** Schleifen **/
			for(int n = 0; n < feld.GetLength(0); n++){
				feld[n] = n;
			}
			
			foreach(int elem in feld){
				Console.WriteLine(elem);
			}
			Console.WriteLine();
			
			int m = 0;
			while(m < feld.GetLength(0)){
				Console.WriteLine(feld[m]);
				m++;
			}
			Console.WriteLine();
			
			m = 0;
			do {
				Console.WriteLine(feld[m]);
				m++;
			} while ( m < feld.GetLength(0));
			Console.WriteLine();
			
			int[,] matrix = new int[2,3];
			for(int i = 0; i < matrix.GetLength(0); i++){
				for(int j = 0; j < matrix.GetLength(1); j++){
					matrix[i,j] = i + j;
				}
			}
			
			for(int i = 0; i < matrix.GetLength(0); i++){
				Console.WriteLine();
				for(int j = 0; j < matrix.GetLength(1); j++){
					Console.WriteLine(matrix[i,j]);
				}
			}
		}

		public void objectInitTest(){
			// with 'init' instead of 'set' you can use object initializers to create an object //
            var p = new Person(){Firstname = "A", Lastname = "B"};
            Console.WriteLine(p.Firstname);
		}

		public void nullableTest(){
			// object? = '?' object kann null sein
            string? d = nullableRefTest();
            Console.WriteLine(d ?? "null");
		}

		private string? nullableRefTest(){
            return null;
        }

		private (string,string,int) getData() {
            // return a tuple of 3 elements
            return ("Name","City",11);
        }

		public void getTupleWithDiscard(){
            // Discard: We are not interested in value, so turn value into underscore (_)
            var (name,city,_) = getData();
            Console.WriteLine($"name={name},city={city}");
        }
	}
}  
