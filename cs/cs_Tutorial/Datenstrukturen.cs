using System;
using System.Collections.Generic;

namespace Tutorial {
    /** Standard Datenstrukturen **/
    public class Datenstrukturen {

        public void listtest(){
            List<Int32> liste = new List<Int32>();
            
            for(Int32 n = 0; n < 10; n++){
                liste.Add(n);
            }

            liste.Insert(1, -1);
            liste.Remove(6);
                
            foreach(Int32 elem in liste){
                Console.WriteLine(elem);
            }
        }
        
        public void hashsettest(){
            HashSet<Int32> a = new HashSet<Int32>();
            a.Add(1);
            a.Add(2);
            a.Add(3);
            a.Add(4);
            a.Add(5);
            
            HashSet<Int32> b = new HashSet<Int32>();
            b.Add(1);
            b.Add(3);
            b.Add(5);
            b.Add(7);
            b.Add(9);

            HashSet<Int32> intersection = a;
            intersection.IntersectWith(b);
            
            foreach(Int32 elem in intersection){
                Console.WriteLine(elem);
            }
        }
        
        public void maptest(){
            Dictionary<Int32, string> plz = new Dictionary<Int32, string>();
            plz.Add(1, "Stadt");
            plz.Add(2, "Dorf");
            plz.Add(3, "Hauptstadt");
            
            if(plz.ContainsKey(2)){
                Console.WriteLine(plz[2]);
            }
            
            foreach( KeyValuePair<Int32, string> k in plz){
                Int32 key = k.Key;
                string val = k.Value;
                Console.WriteLine("{0} - {1}", key, val);
            }
        }
        
    }
} 
