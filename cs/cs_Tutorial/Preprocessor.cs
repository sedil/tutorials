#define DEBUG
#undef DEBUG

using System;

namespace Tutorial {

    public class Preprocessor {

        public Preprocessor() { }

        public bool isDebugActivated(){
#if (DEBUG)
            return true;
#else
            return false;
#endif
        }

        public int replaceMeAtSoonAsPossible(){
#if (DEBUG)
#warning Deprecated code in this function
            return 1;
#else
            return 0;
#endif
        }

    }
}