using System;
using System.Collections.Generic;

namespace Tutorial {

    class Functional {

        public Functional() { }

        /* Diese Funktion kann mit allen Lambdas des Typs:
           Func<List<Int32>,Int32> aufgerufen werden */
        public Int32 Sum(List<Int32> list){
            Int32 sum = 0;
            foreach(Int32 elem in list){
                sum += elem;
            }
            return sum;
        }

        public void TestLambda(){
            List<Int32> list = new List<Int32>();
            for(Int32 i = 0; i < 10; i++){
                list.Add(i+1);
            }

            Int32 sum = sumupFilteredList(list, 5);
            Console.WriteLine(sum);
        }

        public Int32 sumupFilteredList(List<Int32> list, Int32 filtercondition){
            /* FindAll ist ein vorgefertigter Lambda auf Listen */
            List<Int32> filter = list.FindAll(elem => elem >= filtercondition);

            /* Mit Func/Action kann man selber ein Lambda schreiben.
               Mit diesen Lambda kann man alle Funktionen aufrufen, die den selben
               Funktionsheader haben wie Func */
            Func<List<Int32>,Int32> lambda = Sum;
            return lambda(filter);

            // Oder als einzeiler...
            // return Sum(list.FindAll(elem => elem >= filtercondition));
        }

    }
}