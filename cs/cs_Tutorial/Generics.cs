using System;
using System.Collections.Generic;

namespace Tutorial {

    /** Generics **/
    public class Vektor<T> {

        private T x, y;
        
        public T getX {
            get {
                return x;
            }
        }
        
        public T getY {
            get {
                return y;
            }
        }
        
        public Vektor(T x, T y){
            this.x = x;
            this.y = y;
        }
        
        /** Lambda : Func<in,out> lambda = variable  => operation **/
        public static Func<Vektor<int>, int> coordsum = vector => (vector.x + vector.y);
    }
}
