/* vgl. struct */
data class Product(
    val id : Int, 
    val name : String, 
    val price : Float
)

fun dataclassTest(){
   	val apfel = Product(1,"apfel",0.19f);
    val banane = Product(2,"banane",0.39f);
    val (id,name,price) = apfel;
    println(id);
    println(apfel == banane);
    println(apfel); 
} 
