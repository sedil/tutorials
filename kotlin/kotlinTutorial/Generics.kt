/* Generische Klasse inklusiv einer oberen Grenze */
class Container<T> where T : Number {
    public var liste : MutableList<T> = mutableListOf();
    
    public fun addItem(item : T){
        liste.add(item);
    }
    
    public fun getFirstItem() : T {
        return liste[0];
    }
}

/* Generische Funktion ausserhalb von Klassen */
fun <S>genListOf(x : S) : List<S> {
    return listOf<S>(x);
}

fun main() {
    val intList = genListOf<Int>(1);
    
	var l = Container<Int>();
    l.addItem(3);
    l.addItem(5);
    println(l.getFirstItem());
}
