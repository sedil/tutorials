interface Implementable {
    
    fun addOne(x : Int) : Int;
}

/* open = Klasse ist vererbbar, ohne open nicht */
/* abstract = Klasse kann nicht instanziiert werden */
abstract public class Figur(name : String){
    private var name : String = name;
    
    /* kann nur von Subklassen aufgerufen werden */
    /* open = kann überschrieben werden */
    protected open fun getFigurename() : String {
        return this.name;
    }
    
    /* muss in Subklassen überschrieben werden */
    protected abstract fun mustBeOverridden();
}

public class Point(x : Int, y : Int) : Figur("Figur"), Implementable {
    private val name : String = "Point";
    
    public var x : Int = x
    get() = field
    set(value) {
        if (value < 0 ){
            field = 0;
        } else {
            field = value;
        }
    }
    
    public var y : Int = y
    get() = field
    set(value) {
        if (value < 0 ){
            field = 0;
        } else {
            field = value;
        }
    }

    init { }
    
    /* Operator überladen */
    operator fun plus(p : Point) : Point {
        return Point(this.x + p.x, this.y + p.y);
    }
    
    public fun getCoordinates() : Array<Int> {
        return arrayOf(this.x,this.y);
    }
    
    public override fun getFigurename() : String {
        return this.name;
    }
    
    protected override fun mustBeOverridden(){
        println("example");
    }
    
    /* Funktion(en) aus dem Interface */
    override fun addOne(x : Int) : Int {
        return x + 1;
    }
}

    /* Typecast */
    // var x : Zieltyp? = null;
    // if( actualObject is Zieltyp){
    //     x = actualObject as Zieltyp;
    // }

fun main() {
	var p : Point = Point(3,4);
    println(p.x)
    println(p.getFigurename());
    var q : Point = Point(7,6);
    var r = p + q;
}
