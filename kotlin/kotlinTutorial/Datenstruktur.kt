class Datenstruktur {
    
    public fun liste(){
        var liste : List<Int> = listOf(1,2);
        /* hinzufügen und löschen */
        liste += 3;
        liste += 4;
        liste -= 3;
        
        /* lesen */
        println(liste.getOrElse(3){"not found"});
        println(liste[0]);
        
        /* partitionieren */
        val part = liste.partition{it % 2 == 0};
        println(part.component1());
        
        /* liste teilweise behalten: hier die ersten 2 elemente */
        val take = liste.take(2);
        println(take);
        
        /* map mit lambda */
        val newList = liste.map{it -> it*it};
        println(newList);
        
        /* liste filtern */
        val filterList = liste.filter{ it -> it < 2};
        println(filterList);
    }
    
    public fun sets(){
        var menge = hashSetOf(1,2,3,4,5,2);
        println(menge)
    }
    
    public fun map(){
        var myMap : Map<Int, Float> = hashMapOf(4 to 4.1f);
        
        /* element hinzufügen und löschen */
        val pair : Pair<Int, Float> = Pair(3,3.15f);
        myMap = myMap + pair;
        myMap = myMap + hashMapOf(2 to 2.71f);
        myMap - pair;
        
   		/* map auslesen */
        println(myMap[2]);
  
       	var entries = myMap.entries;
        for(elem in entries){
            println(elem.key);
            println(elem.value);
        }
        
    }
    
}

fun main(args : Array<String>) {
	var d = Datenstruktur();
    d.map();
}
