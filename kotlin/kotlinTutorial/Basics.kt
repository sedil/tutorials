/* Exceptions */
class CustomException(message : String) : Exception(message);

fun errorCaller(){
    try {
    	error();
    } catch (e : CustomException){
        println(e);
    }
}

fun error() {
    throw CustomException("int fehler");
}

/* Schleifen und Array */
public fun loops() : Array<Int> {
    var arr = arrayOf(1,2,3,4);
    for(elem in arr){
        println(elem);
    }
    
    for(i in arr.indices){
        println(arr[i])
    }
    
    arr.forEach{println(it)} /* for each bei containern, it=iterator */
    
    var idx : UByte = 5; /* Unsigned Typen möglich */
    while(idx >= 0){
        print(".");
        idx--;
    }
    
    return arr;
}

fun datei(){
    /* Datei lesen */
    try {
    	var content : String = File("input").forEachLine(Charsets.UTF_8){println(it)};
    } catch ( e : Exception ){
        println(e);
    } finally {
        println("wird immer ausgeführt");
    }
    
    /* Datei schreiben */
    val o : String = "output";
    File("output").writeText(o);
}

public fun array2D(){
    var x1 = arrayOf(1,2,3);
    var x2 = arrayOf(4,5,6);
    var x3 = arrayOf(7,8,9);
    var matrix = arrayOf(x1,x2,x3);
    
    matrix[0][2] = 0;
    
    for(row in matrix){
        for(elem in row){
            println(elem);
        }
    }
}

/* Aufbau einer Funktion */
/* y : Int = 0 ist ein Defaultwert, wird bei Parameterübergabe mit y=<Wert> überschrieben */
public fun add(x : Int, y : Int = 0) : Boolean {
    if ( x < y ){
        return true;
    } else {
        return false;
    }
}

/* Besonderheiten in Kotlin */
fun special(){
    var x : String? = null; /* Type? = Datentyp darf null sein */
    println(x?.length); /* Variable? = Zugriff darauf */
    var l : Int = x?.length ?:-1; /* wenn x null ist, dann nehme den Wert hinter ?:<Wert> an
    
    /* variablen swappen */
    var a : Int = 1;
    var b : Int = 2;
    a = b.also{b=a};
    
    /* String template :$variable */
    var a : Int = 1;
    println("Wert = $a als Int");
    
    /* Ranges */
    for(i in 0 ... 5){ /* 5 downTo 0 = runterzählen, auch möglich: step 2 = Nur alle zwei Indizes */
        print(i);
    }
    
    /* lambda expressions */
    val stringLength = {word1 : String, word2 : String -> word1.length + word2.length };
    val result = stringLength("test", "west");
}

/* Funktionen höherer Ordnung */
fun callHighOrderFunction(){
    /* Definiere eine Funktion und was sie machen soll */
	val add = {x : Int, y : Int -> x+y};
    higherOrderFunction(add, 3, 3);
    
    val sub = {x : Int, y : Int -> x-y};
    higherOrderFunction(sub, 3, 3);
}

/* Eine "Funktionenschablone" mit Parameter übergeben */
fun higherOrderFunction(f : (x : Int, y : Int) -> Int, a : Int, b : Int){
    val result = f(a,b);
    println(result);
}

/*Einzeilige Funktion : man spart return */
fun einzeilig(x : Int) = x + x;

fun main(args : Array<String>) {
    val result = add(3,4); /* val = konstante */
    println(result);
    val arr = loops();
    array2D();
}

Video 113
