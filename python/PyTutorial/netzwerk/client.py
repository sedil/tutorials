import sys
import socket
import threading

class Client:

    def __init__(self, host, port, chatname):
        self._chatname = '[' + chatname + '] : '
        self._sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self._sock.connect((str(host), int(port)))

        iThread = threading.Thread(target=self.sendMessage)
        iThread.daemon = True
        iThread.start()

        while True:
            data = self._sock.recv(1024)
            if not data:
                break
            print(str(data, 'utf-8'))

    def sendMessage(self):
        while True:
            name = bytes(self._chatname, 'utf-8')
            message = bytes(input(''), 'utf-8')
            self._sock.send(name + message)
