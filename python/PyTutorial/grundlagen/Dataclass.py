from dataclasses import dataclass

@dataclass()
class Person:
    firstname : str
    lastname : str
    age : int
    sex : bool

    # defaultvalue only as last
    verified : bool = False

    # optional set 'datatype' in arguments and returnvalues
    def verify(self, verified : bool):
        self.verified = verified

    def fullname(self) -> str:
        return f'{self.firstname} {self.lastname}'

if __name__ == '__main__':
    person = Person('First','Last',27,True)
    print(person)
