import sys

from PyQt5.QtWidgets import QApplication, QWidget, QPushButton, QLabel, QVBoxLayout
from PyQt5.QtCore import QThreadPool
from PyQt5.Qt import QRunnable

import time

class Calculate(QRunnable):

    def __init__(self, ui, tasknumber):
        super(Calculate, self).__init__()
        self.ui = ui
        self.tasknumber = tasknumber

    def run(self):
        x = 0
        for _ in range(5):
            self.ui.updateLabel((self.tasknumber, x))
            x += 1
            time.sleep(1)

class ThreadpoolTest(QWidget):

    def __init__(self, parent=None):
        super(ThreadpoolTest, self).__init__(parent)
        self.initComponents()
        self.setLayout(self._layout)
        self.setGeometry(200, 200, 320, 160)
        self.show()

    def initComponents(self):
        self.pool = QThreadPool.globalInstance()
        self.pool.setMaxThreadCount(2)

        self._layout = QVBoxLayout()
        self.button = QPushButton("calculate...")
        self.button.clicked.connect(lambda : self.startThread())
        self.label = QLabel()

        self._layout.addWidget(self.button)
        self._layout.addWidget(self.label)

    def updateLabel(self, msg):
        self.label.setText(str(msg))

    def startThread(self):
        self.updateLabel("start Threadpool...")
        time.sleep(1)
        for task in range(3):
            calc = Calculate(self, task)
            self.pool.start(calc)
        #self.pool.waitForDone()
        self.label.setText("finish Threadpool...")

if __name__ == "__main__":
    app = QApplication(sys.argv)
    gui = ThreadpoolTest()
    sys.exit(app.exec_())