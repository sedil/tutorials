class LambdaUtil:

    def consumer(self, x, expr):
        expr(x)

    def biConsumer(self, x, y, expr):
        expr(x,y)

    def triConsumer(self, x, y, z, expr):
        expr(x,y,z)

    def function(self, x, expr):
        return expr(x)

    def biFunction(self, x, y, expr):
        return expr(x,y)

    def triFunction(self, x, y, z, expr):
        return expr(x,y,z)

class LambdaTest:

    def lambdaTest():
        addLambda = lambda x,y : x+y
        print(addLambda(2,7))

        ''' <True> if <Condition> else <False> '''
        conditionLambda = lambda x : "negativ" if x < 0 else "even" if x % 2 == 0 else "odd"
        print(conditionLambda(3))

        ''' list comprehension [returnlist <for-in loop> <condition>]'''
        result = [elem for elem in [1,2,3,4,5] if elem % 2 == 0]
        print(result)

        ''' filter(lambda, liste)'''
        result = list(filter(lambda x : x%3==0, [2,4,6]))
        print(result)

        ''' map(lambda, liste)'''
        result = list(map(lambda x : x**2,[1,2,3,4]))
        print(result)
