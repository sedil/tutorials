import time
import threading

class CounterThread(threading.Thread):

    continueList = [0,1]

    def __init__(self, id):
        threading.Thread.__init__(self)
        self._id = id

    def sumUp(self, idx):
        next = CounterThread.continueList[idx-1] + CounterThread.continueList[idx]
        CounterThread.continueList.append(next)

    def run(self):
        lockMe.acquire()
        print("Starte " + str(self._id))
        for i in range(8):
            self.sumUp(len(CounterThread.continueList) - 1)

        lockMe.release()
        print('Ende ' + str(self._id))

lockMe = threading.Lock() # Als globale Variable
t1 = CounterThread(1)
t2 = CounterThread(2)
t1.start()
t2.start()
t1.join()
t2.join()

print(CounterThread.continueList[6])
