import re

'''
A class to count the appearances of words from a text file
'''
class Wordcounter:

    @staticmethod
    def processTextfile(path):
        if not path:
            return

        file = open(path, 'r')
        content = ''
        for line in file:
            content += line

        ''' delete all non characters and convert all characters in 
        lowercase to avoid duplicates '''
        content = re.sub('\W', ' ', content)
        content = content.lower()
        wordlist = content.split(' ')

        ''' remove empty words and words with only one character '''
        for elem in wordlist:
            if len(elem) <= 1:
                wordlist.remove(elem)
        return wordlist

    @staticmethod
    def countWordoccurrences(wordlist: list):
        wordmap = {}
        for elem in wordlist:
            if len(elem) == 0:
                continue
            ''' word not in map '''
            if not wordmap.get(elem):
                wordmap.update({elem: 1})
            else:
                ''' word in map, increase wordcounter '''
                value = wordmap[elem]
                wordmap.update({elem: value + 1})
        return wordmap

    ''' sort all words with their appearances in discending order '''

    @staticmethod
    def sortWordoccurrences(wordmap: dict) -> list:
        sortedWords = []
        max = 0
        for value in wordmap.values():
            if max < value:
                max = value

        maxCounter = max
        for n in range(maxCounter):
            for key, value in wordmap.items():
                if value == max:
                    sortedWords.append((key, value))
            max = max - 1
        return sortedWords


if __name__ == "__main__":
    wordlist = Wordcounter.processTextfile('/home/sebastian/Schreibtisch/wordlist')
    wordmap = Wordcounter.countWordoccurrences(wordlist)
    wordlist = Wordcounter.sortWordoccurrences(wordmap)
    for elem in wordlist:
        print(elem)
