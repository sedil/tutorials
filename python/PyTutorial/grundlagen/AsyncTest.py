import asyncio

async def insertNumber(number, queue):
    await asyncio.sleep(2)
    await queue.put(number)
    print(f'producer with content {number} done')

async def printNumber(queue):
    while True:
        await asyncio.sleep(1)
        task = await queue.get()
        print(f'consumer has obtained {task}')
        queue.task_done()

async def main():
    queue = asyncio.Queue() # Queue, um Tasks hinein zu speichern
    producerlist = [asyncio.create_task(insertNumber(number, queue)) for number in range(5)]
    consumerlist = [asyncio.create_task(printNumber(queue)) for _ in range(5)]
    await asyncio.gather(*producerlist) # Führe alle Tasks aus dieser Liste aus
    await queue.join() # Warte bis alle Proucers fertig sind bevor Konsumiert wird
    for elem in consumerlist:
        elem.cancel('consumer done')

if __name__ == "__main__":
    asyncio.run(main()) # Eintrittspunkt
