class Algorithm:

    @staticmethod
    def sortCharactersAscending(text):
        textchars = list(text)
        charlist = []
        for _ in range(len(textchars)):
            min = 'z'
            for elem in textchars:
                if elem <= min:
                    min = elem
            textchars.remove(min)
            charlist.append(min)
        return charlist

    @staticmethod
    def binarySearch(list, target, start, end):
        if end < start:
            return 'not found'
        mid = int((start + end) / 2)
        if list[mid] == target:
            return target
        elif list[mid] < target:
            return Algorithm.binarySearch(list, target, mid + 1, end)
        else:
            return Algorithm.binarySearch(list, target, start, mid - 1)

    @staticmethod
    def binarySearchStart(list, target):
        return Algorithm.binarySearch(list, target, 0, len(list) - 1)

    @staticmethod
    def reheap(values, n, i):
        maximum = i
        left = 2*i+1
        right = 2*i+2

        if left < n and values[i] < values[left]:
            maximum = left
        if right < n and values[maximum] < values[right]:
            maximum = right
        if maximum != i:
            temp = values[i]
            values[i] = values[maximum]
            values[maximum] = temp
            Algorithm.reheap(values, n, maximum)

    @staticmethod
    def heapSort(values):
        n = len(values)
        for i in range(n, -1, -1):
            Algorithm.reheap(values, n, i)
        for i in range(n - 1, 0, -1):
            values[i], values[0] = values[0], values[i]
            Algorithm.reheap(values, i, 0)


if __name__ == "__main__":
    #print(Algorithm.sortCharactersAscending('sayaka'))
    #print(Algorithm.binarySearchStart([0,1,2,3,4,5,6,7,8,9], 4))
    arr = [3,7,1,14,11,1,5,0,6,9]
    Algorithm.heapSort(arr)
    for elem in arr:
        print(elem)