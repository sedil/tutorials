class Cast:

    def checkType(self, attr) -> str :
        return type(attr)

    def boolToInt(self, boolval) -> int :
        if boolval == True:
            return 1
        else:
            return 0
        
    def boolToFloat(self, boolval) -> float :
        if boolval == True:
            return 1.0
        else:
            return 0.0
        
    def boolToString(self, boolval) -> str :
        if boolval == True:
            return 'True'
        else:
            return 'False'
        
    def intToFloat(self, intval) -> float :
        return float(intval)
    
    def intToString(self, intval) -> str :
        return str(intval)
    
    def floatToInt(self, floatval) -> int :
        return int(floatval)
    
    def floatToString(self, floatval) -> str :
        return str(floatval)
    
    def stringToInt(self, stringval) -> int :
        if stringval.isdigit():
            return int(stringval)
        else:
            return -1
        
    def stringToFloat(self, stringval) -> float :
        try:
            return float(stringval)
        except ValueError:
            return -1.0
