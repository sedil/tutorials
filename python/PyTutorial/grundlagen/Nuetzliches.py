import sys

def nuetzliches():
    # Formatted String (normal, formatted, newformatted
    x = 5
    y = 3
    print("x=",x,"y=",y)
    print(f"x={x}, y={y}")
    print(f"{x=}, {y=}")

    # zip : Mehrere Datenstrukturen verschmelzt ausgeben oder zugreifen
    zahl = [1,2,3,4,5]
    buch = ('a','b','c')
    for tupel in zip(zahl, buch):
        print(tupel)

    # zeige Funktionen zu Objekt an
    print(dir(list))

    # list comprehention: Beispiel
    z = [a + b # result part
         for a in [1,2,3,4,5] # nested list is possible
         for b in [-1,2]
         if b > 0 and a < 5] # if condition is possible too
    print(z)

    # Formatted String (normal, formatted, newformatted
    x = 5
    y = 3
    print("x=",x,"y=",y)
    print(f"x={x}, y={y}")
    print(f"{x=}, {y=}")

    # .join Beispiel
    names = ['sedile','sayaka','olivia']
    text = ' '.join(names)
    print(text)

    # Speicherbelegung
    print(sys.getsizeof(names))
    
    # walrus operator
    f = open('','r')
    while line := f.readline():
        print(line)
