import re

def regexExamples():
    text = "I want visit: Tokyo Tower 2 times on this evening at 1930, because it costed .95."

    pattern = re.compile(r'Tok[i|y]o') # search for i or y
    a = pattern.search(text)
    print(a)

    pattern = re.compile(r'\d+') # search for all digits with at least 1 length and return as a list [0-9]
    a = pattern.findall(text)
    print(a)

    pattern = re.compile(r'\d*\.\d\d') # search for Pattern : digits ( 0 or more) . digit digit
    a = pattern.findall(text)
    print(a)

    pattern = re.compile(r'\w?[o]\w*') # search for pattern : <optional> + o + more characters
    a = pattern.findall(text)
    print(a)

    pattern = re.compile(r'[a-zA-Z]{5,6}') # all words with a length between 5 and 6 and no digits
    a = pattern.findall(text)
    print(a)

    pattern = re.compile(r'\".*\"') # all text between citation
    a = pattern.findall('Sayaka said: "I am a Miko!" to me.')
    print(a)

    mail = re.findall(r'([a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.(de|com|uk|jp))','My mail is: ghost34@gala.uk')
    print(mail)
    
    url = re.findall(r'http[s]?://(?:[a-zA-Z]|[0-9]|[$-_@.&+]|[!*\(\),]|(?:%[0-9a-fA-F][0-9a-fA-F]))+','url: https://ftp.tt.jp')
    print(url)

    # (...) = group, a piece of pattern
    pattern = re.compile('([0-9]+)-([0-9]+) ([a-z]): ([a-z]+)')
    line = '2-10 z: vghqbzbcxf'
    match = re.search(pattern, line)
    min_amount = int(match.group(1))
    max_amount = int(match.group(2))
    char = match.group(3)
    password = match.group(4)
    print(min_amount, max_amount, char, password)