class Klasse:

    def __init__(self,stufe):
        self._stufe = stufe
        self._schuelerliste = []

    def addSchueler(self,schueler):
        self._schuelerliste.append(schueler)

    def printSchueler(self):
        for p in self._schuelerliste:
            print(p.getName() + ' ' + str(p.getAlter()) + ' ' + str(p.getGeschlecht()))

    def getStufe(self):
        return self._stufe
