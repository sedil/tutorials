from objektorientierung.Lehrer import Lehrer
from objektorientierung.Schueler import Schueler
from objektorientierung.Klasse import Klasse

if __name__ == "__main__":
    s1 = Schueler('Ingo',13,False)
    s2 = Schueler('Jutta',15,True)
    s3 = Schueler('Tobias',20,False)
    s4 = Schueler('Bianca',17,True)

    k1 = Klasse(7)
    k1.addSchueler(s1)
    k1.addSchueler(s2)
    k2 = Klasse(8)
    k2.addSchueler(s3)
    k2.addSchueler(s4)

    l1 = Lehrer('Gueting',36,False)
    l1.setKlasse(k1)
    l2 = Lehrer('Weidel',45,True)
    l2.setKlasse(k2)

    guetingInfo = l1.getName() + ' ' + str(l1.getAlter()) + ' ' + str(l1.getGeschlecht())
    print(guetingInfo)

    klasse = l2.getKlasse()
    print('Jahrgang : ' + str(klasse.getStufe()))
    klasse.printSchueler()
    
    
