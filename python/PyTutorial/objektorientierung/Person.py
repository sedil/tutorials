from abc import abstractmethod

class Person:

    def __init__(self,name,alter,geschlecht):
        self._name = name
        self._alter = alter
        self._geschlecht = geschlecht

    @abstractmethod
    def setPersonID(self,pid):
        pass

    @abstractmethod
    def getPersonID(self):
        pass

    def getName(self):
        return self._name

    def getAlter(self):
        return self._alter

    def getGeschlecht(self):
        return self._geschlecht
