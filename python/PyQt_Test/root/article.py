class Article():

    def __init__(self, article_id, name, value, amount):
        self.__id = article_id
        self.__name = name
        self.__value = value
        self.__amount = amount
        
    def takeArticle(self, amount) -> bool:
        if amount <= 0:
            print('ERROR: negative value:',amount)
            return False
        
        if self.__amount - amount >= 0:
            self.__amount -= amount
            print(self.__amount,'available of',self.__name)
            
            if self.__amount == 0:
                print('please order:',self.__name)
            
            return True
        else:
            print('please order',self.__name,'because there is none left')
            return False
        
    def addArticle(self, amount) -> bool:
        if amount <= 0:
            print('ERROR: negative value:',amount)
            return False
        
        if self.__amount + amount > 10:
            print('too many of',self.__name,'max. Capacity is 10')
            return False
        else:
            self.__amount += amount
            print('added',amount,'of',self.__name)

    def getTotalValue(self) -> float:
        return self.__amount * self.__value
        
    def getArticleID(self) -> int:
        return self.__id

    def getArticleName(self) -> str:
        return self.__name
    
    def getArticleValue(self) -> float:
        return self.__value
        
    def getArticleAmount(self) -> int:
        return self.__amount
    
    def __str__(self):
        return self.__id,self.__name,self.__value,self.__amount
        
        
        