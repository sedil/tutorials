import re
from root.storagefilemanager import StorageFileHandler
from root.article import Article

class ArticleManager():

    def __init__(self, storagefile):
        self.__storagelist = StorageFileHandler()
        self.__storagelist.readStorageFile(storagefile)
        self.__articlelist = self.__storagelist.getArticleListFromFile()
        
    def addNewArticle(self, article_id, article_name, article_value, article_amount):
        (available, article) = self.searchArticleByID(article_id)
        if not available:
            self.__articlelist.addArticle(Article(article_id, article_name, article_value, article_amount))
        else:
            print('article ID',article_id,'is used')
        
    def removeArticle(self, article_id):
        (available, article) = self.__articlelist.searchArticleByID(article_id)
        if available:
            self.__articlelist.removeArticle(article)
        else:
            print('article with ID',article_id,'is not listed')
        
    def orderArticle(self, article_id, amount):
        (available, article) = self.__articlelist.searchArticleByID(article_id)
        if available:
            article.addArticle(amount)
        else:
            print('article is not listed')
            
    def sellArticle(self, article_id, amount):
        (available, article) = self.__articlelist.searchArticleByID(article_id)
        if available:
            article.takeArticle(amount)
        else:
            print('article is not listed')
            
    def searchArticleByID(self, article_id) -> tuple:
        return self.__articlelist.searchArticleByID(article_id)
    
    def searchArticleByPattern(self, searchword) -> list:
        result = []
        for elem in self.__articlelist.getArticleList():
            if re.search(searchword, elem.getArticleName()):
                result.append(elem)
        return result
    
    def stocktaking(self):
        for i in self.__articlelist.getArticleList():
            print((i.getArticleID(),i.getArticleName(),i.getArticleAmount()))
            
    def saveStorageFile(self,storagefile):
        self.__storagelist.saveStorageFile(storagefile)     
    
    def listAllArticles(self) -> list:
        return self.__articlelist.getArticleList()
        
        
        