import json
from root.articlelist import ArticleList
from root.article import Article

class StorageFileHandler(object):

    def __init__(self):
        self.__articles = ArticleList()
    
    def readStorageFile(self, file):
        with open(file,'r') as jsonfile:
            data = json.load(jsonfile)
            
            for elem in data['article_list']:
                article = Article(elem['article_id'],elem['article_name'],elem['article_value'],elem['article_amount'])
                self.__articles.addArticle(article)
        print('read storagefile successful')
    
    def saveStorageFile(self, file):
        data = {"article_list" : []}
        for elem in self.__articles.getArticleList():
            value = {"article_id" : elem.getArticleID(),
                      "article_name" : elem.getArticleName(),
                      "article_value" : elem.getArticleValue(),
                      "article_amount" : elem.getArticleAmount()}
            data['article_list'].append(value)
        
        convert = str(data).replace("'", "\"", -1)
        json.dumps(convert)
        
        with open(file,'w') as jsonfile:
            jsonfile.write(convert)
        print('storagefile saved')
    
    def getArticleListFromFile(self):
        return self.__articles

        