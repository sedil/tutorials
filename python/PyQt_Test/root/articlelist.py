class ArticleList():

    def __init__(self):
        self.__articlelist = set()
        
    def addArticle(self,article):
        self.__articlelist.add(article)
        
    def removeArticle(self,article):
        self.__articlelist.remove(article)
        
    def searchArticleByID(self, article_id) -> tuple:
        for i in self.__articlelist:
            if i.getArticleID() == article_id:
                return (True,i)
        return (False,None)
    
    def searchArticleByName(self, article_name) -> tuple:
        for i in self.__articlelist:
            if i.getArticleName() == article_name:
                return (True,i)
        return (False,None)

    def countArticles(self) -> int:
        return len(self.__articlelist)
    
    def getArticleList(self) -> set:
        return self.__articlelist

        