import sys
from PyQt5.QtWidgets import QApplication, QWidget, QVBoxLayout, QListWidget,\
    QMenuBar, QMenu, QAction
from gui.storagemodifier import StorageModifier
from gui.changeModifier import ChangeModifier
from root.manager import ArticleManager

class MainFrame(QWidget):
    
    def __init__(self, parent=None):
        super(MainFrame, self).__init__(parent)
        self.__initComponents()
        self.__initLayout()
        self.__connectActions()
        
    def __initLayout(self):
        self.__layout = QVBoxLayout()
        
        self.__layout.addWidget(self.__menubar)
        self.__layout.addWidget(self.__articlelist)
        self.setLayout(self.__layout)
        
        self.setGeometry(128,128,640,360)
        self.setWindowTitle('Simple Storagemanager')
        self.show()

    def __initComponents(self):
        self.__menubar = QMenuBar()
        self.__datamenu = QMenu('&Data',self)
        self.__modifyArticles = QAction('Buy/Sell', self)
        self.__modifyArticles.setEnabled(False)
        self.__newArticle = QAction('add new Article',self)
        self.__removeArticle = QAction('remove Article',self)
        self.__newArticle.setEnabled(False)
        self.__removeArticle.setEnabled(False)
        self.__exitApplication = QAction('Exit',self)
        
        self.__storagemenu = QMenu('&Storage',self)      
        self.__loadArticles = QAction('Read Articlelist',self)
        self.__saveArticles = QAction('Save Articlelist',self)
        self.__saveArticles.setEnabled(False)
        
        self.__datamenu.addAction(self.__modifyArticles)
        self.__datamenu.addAction(self.__newArticle)
        self.__datamenu.addAction(self.__removeArticle)
        self.__datamenu.addAction(self.__exitApplication)
        self.__menubar.addMenu(self.__datamenu)
        
        self.__storagemenu.addAction(self.__loadArticles)
        self.__storagemenu.addAction(self.__saveArticles)
        self.__menubar.addMenu(self.__storagemenu)
        
        self.__articlelist = QListWidget()
        
    def __connectActions(self):
        self.__modifyArticles.triggered.connect(self.__openModifyWindow)
        self.__newArticle.triggered.connect(self.__addNewArticleInStorage)
        self.__removeArticle.triggered.connect(self.__removeArticleInStorage)
        self.__exitApplication.triggered.connect(self.__exit)
        self.__loadArticles.triggered.connect(self.__readArticlesFromJson)
        self.__saveArticles.triggered.connect(self.__saveActualArticleList)
        
    def __readArticlesFromJson(self):
        self.__manager = ArticleManager('/home/sebastian/Dokumente/programming/pythonCode/PyQt_Test/storage.json')
        articles = self.__manager.listAllArticles()
        self.__articlelist.clear()
        for elem in articles:
            self.__articlelist.addItem(elem.getArticleName())
        self.__saveArticles.setEnabled(True)
        self.__modifyArticles.setEnabled(True)
        self.__newArticle.setEnabled(True)
        self.__removeArticle.setEnabled(True)
        
    def __updateList(self):
        self.__articlelist.clear()
        articles = self.__manager.listAllArticles()
        for elem in articles:
            self.__articlelist.addItem(elem.getArticleName())
            
    def __saveActualArticleList(self):
        self.__manager.saveStorageFile('/home/sebastian/Dokumente/programming/pythonCode/PyQt_Test/savetest.json')

    def __openModifyWindow(self):
        modwindow = StorageModifier(self)
        modwindow.setManager(self.__manager)
        modwindow.exec_()
        self.__updateList()
     
    def __addNewArticleInStorage(self):
        changewindow = ChangeModifier(self)
        changewindow.setManager(self.__manager, True)
        changewindow.exec_()
        self.__updateList()
        
    def __removeArticleInStorage(self):
        changewindow = ChangeModifier(self)
        changewindow.setManager(self.__manager, False)
        changewindow.exec_()
        self.__updateList()

    def __exit(self):
        sys.exit()

if __name__ == "__main__":
    app = QApplication(sys.argv)
    frame = MainFrame()
    sys.exit(app.exec_())
    
    