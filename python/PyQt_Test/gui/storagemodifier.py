import sys
from PyQt5.QtWidgets import QGridLayout, QVBoxLayout, QDialog, QPushButton, QLabel, QLineEdit

class StorageModifier(QDialog):

    def __init__(self, parent=None):
        super(StorageModifier, self).__init__(parent)
        self.__manager = None
        self.__initComponents()
        self.__initLayout()
        self.__connect()
        
    def __initLayout(self):
        self.__vbox = QVBoxLayout()
        
        self.__modify_grid = QGridLayout()
        self.__modify_grid.addWidget(self.__orderArticleBtn,0,0)
        self.__modify_grid.addWidget(self.__takeArticleBtn,0,1)
        self.__modify_grid.addWidget(self.__searchbtnName,2,0)
        self.__modify_grid.addWidget(self.__searchbtnID,2,1)
        self.__modify_grid.addWidget(self.__modifyAmount,3,0)
        self.__modify_grid.addWidget(self.__modifyInput,3,1)
        self.__modify_grid.addWidget(self.__articleLabel,4,0)
        self.__modify_grid.addWidget(self.__modifyArticle,4,1)
        
        self.__vbox.addLayout(self.__modify_grid)
        
        self.__info_grid = QGridLayout()
        self.__info_grid.addWidget(self.__idLabelStatic,0,0)
        self.__info_grid.addWidget(self.__idLabel,1,0)
        self.__info_grid.addWidget(self.__amountLabelStatic,0,1)
        self.__info_grid.addWidget(self.__amountLabel,1,1)
        self.__info_grid.addWidget(self.__nameLabelStatic,0,2)
        self.__info_grid.addWidget(self.__nameLabel,1,2)
        self.__info_grid.addWidget(self.__valueLabelStatic,0,3)
        self.__info_grid.addWidget(self.__valueLabel,1,3)

        self.__vbox.addLayout(self.__info_grid)
        self.setLayout(self.__vbox)
        self.setWindowTitle('Modify Storage')
        self.show()
        
    def __initComponents(self):
        self.__orderArticleBtn = QPushButton('Order Article',self)
        self.__takeArticleBtn = QPushButton('Take Article',self)
        self.__searchbtnName = QPushButton('Search Article By Name',self)
        self.__searchbtnID = QPushButton('Search Article By ID',self)
        
        self.__modifyAmount = QLabel('Amount / Article-ID',self)
        self.__modifyInput = QLineEdit('Zahl/ID',self)
        self.__articleLabel = QLabel('Articlename',self)
        self.__modifyArticle = QLineEdit('Name',self)
        
        self.__idLabelStatic = QLabel('Article ID',self)
        self.__amountLabelStatic = QLabel('Amount',self)
        self.__nameLabelStatic = QLabel('Name',self)
        self.__valueLabelStatic = QLabel('Value',self)
        
        self.__idLabel = QLabel(self)
        self.__amountLabel = QLabel(self)
        self.__nameLabel = QLabel(self)
        self.__valueLabel = QLabel(self)
    
    def setManager(self, manager):
        self.__manager = manager
    
    def __connect(self):
        self.__searchbtnName.clicked.connect(self.__searchArticleByName)
        self.__searchbtnID.clicked.connect(self.__searchArticleByID)
        self.__orderArticleBtn.clicked.connect(self.__orderArticle)
        self.__takeArticleBtn.clicked.connect(self.__takeArticle)
    
    def __orderArticle(self):
        if self.__examineAmountInput() and self.__examineArticleNameInput():
            try:
                art_id = int(self.__modifyArticle.text())
                art_amount = int(self.__modifyInput.text())
                self.__manager.orderArticle(art_id,art_amount)
            except ValueError:
                print(sys.exc_info()[0])
                print('article id is not a number')
    
    def __takeArticle(self):
        if self.__examineAmountInput() and self.__examineArticleNameInput():
            try:
                art_id = int(self.__modifyArticle.text())
                art_amount = int(self.__modifyInput.text())
                self.__manager.sellArticle(art_id,art_amount)
            except ValueError:
                print(sys.exc_info()[0])
                print('article id is not a number')
    
    def __searchArticleByName(self):
        if self.__examineArticleNameInput():
            result = self.__manager.searchArticleByPattern(self.__modifyArticle.text())
            if result:

                # get only first element
                self.__idLabel.setText( str(result[0].getArticleID()) )
                self.__amountLabel.setText( str(result[0].getArticleAmount()) )
                self.__nameLabel.setText( str(result[0].getArticleName()) )
                self.__valueLabel.setText( str(result[0].getArticleValue()) )
            else:
                print('not such an article available')
    
    def __searchArticleByID(self):
        if self.__examineAmountInput():
            (available, article) = self.__manager.searchArticleByID(int(self.__modifyInput.text()))
            print((available, article.getArticleID()))
            if available:

                # get only first element
                self.__idLabel.setText( str(article.getArticleID()) )
                self.__amountLabel.setText( str(article.getArticleAmount()) )
                self.__nameLabel.setText( str(article.getArticleName()) )
                self.__valueLabel.setText( str(article.getArticleValue()) )
            else:
                print('unknown ID')

    def __examineAmountInput(self) -> bool:
        amount = self.__modifyInput.text()
        
        if not amount.strip():
            print('amount is empty or has whitespaces')
            return False
        else:
            try:
                amount = int(amount)
                return True                
            except ValueError:
                print(sys.exc_info()[0])
                print('amount input is not numeric')
                return False                
                
    def __examineArticleNameInput(self) -> bool:
        name = self.__modifyArticle.text()
        if not name.strip():
            print('name input is empty or has whitespaces')
            return False
        else:
            return True     
        
        
        