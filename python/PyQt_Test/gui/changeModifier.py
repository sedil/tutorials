import sys
from PyQt5.QtWidgets import QVBoxLayout, QHBoxLayout, QLabel, QLineEdit, QPushButton, QDialog

class ChangeModifier(QDialog):

    def __init__(self, parent=None):
        super(ChangeModifier, self).__init__(parent)
        self.__manager = None
        self.__addNew = False
        self.__initComponents()
        self.__initLayout()
        self.__connect()
        
    def __initLayout(self):
        self.__layout = QVBoxLayout()
        
        self.__leftbox = QHBoxLayout()
        self.__rightbox = QHBoxLayout()
        
        self.__leftbox.addWidget(self.__articleIDLabel)
        self.__leftbox.addWidget(self.__articleNameLabel)
        self.__leftbox.addWidget(self.__articleValueLabel)
        self.__leftbox.addWidget(self.__articleAmountLabel)
        
        self.__rightbox.addWidget(self.__inputID)
        self.__rightbox.addWidget(self.__inputName)
        self.__rightbox.addWidget(self.__inputValue)
        self.__rightbox.addWidget(self.__inputAmount)
        
        self.__layout.addWidget(self.__confirmBtn)
        self.__layout.addLayout(self.__leftbox)
        self.__layout.addLayout(self.__rightbox)
        
        self.setLayout(self.__layout)
        self.setWindowTitle('Add/Remove Article')
        self.show()
    
    def __initComponents(self):
        self.__articleIDLabel = QLabel('Article-ID')
        self.__articleNameLabel = QLabel('Article Name')
        self.__articleValueLabel = QLabel('Value')
        self.__articleAmountLabel = QLabel('Amount')
        
        self.__inputID = QLineEdit()
        self.__inputName = QLineEdit()
        self.__inputValue = QLineEdit()
        self.__inputAmount = QLineEdit()
        
        self.__confirmBtn = QPushButton()
            
    def __changeArticle(self):
        if self.__examineInput():
            if self.__addNew:
                self.__manager.addNewArticle(int(self.__inputID.text()),
                                             self.__inputName.text(),
                                             float(self.__inputValue.text()),
                                             int(self.__inputAmount.text()))
            else:
                self.__manager.removeArticle(int(self.__inputID.text()))
        else:
            print('some inputs are invalid')
    
    def __connect(self):
        self.__confirmBtn.clicked.connect(self.__changeArticle)
    
    def setManager(self, manager, addNew):
        self.__manager = manager
        self.__addNew = addNew
        if self.__addNew:
            self.__confirmBtn.setText('add new Article')
        else:
            self.__confirmBtn.setText('remove Article')
        
    def __examineInput(self):
        # for remove article
        if not self.__addNew:
            try:
                print(self.__inputID.text())
                article_id = int(self.__inputID.text())
                if article_id < 1:
                    print('negative IDs are invalid')
                    return False
                else:
                    return True
            except ValueError:
                print(sys.exc_info()[0])
                print('id was not a number')
                return False            
        
        else:
            # for add article
            name = self.__inputName.text()
            if not name.strip():
                print('article name was empty or had whitspaces')
                return False
            
            try:
                article_id = int(self.__inputID.text())
                print(article_id)
                value = float(self.__inputValue.text())
                print(value)
                amount = int(self.__inputAmount.text())
                print(amount)
                
                if article_id < 1:
                    print('negative IDs are invalid')
                    return False
                if value <= 0:
                    print('value must be positiv and greater than zero')
                    return False
                if amount <= 0 or amount >= 10:
                    print('amount must be between 1 and 9')
                    return False
                
                return True
            except ValueError:
                print(sys.exc_info()[0])
                print('id,value or amount are not numbers')
                return False
        
        