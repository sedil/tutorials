#pragma once

#include "Figur.h"

class Polygon : public Figur {
private:
    unsigned int punkte;
public:
    explicit Polygon(unsigned int pts);
    virtual ~Polygon();

    virtual unsigned int getAnzahlPunkte() const;
    virtual double getUmfang(double *args) = 0;

    virtual void setParameter(const double &p) = 0;
    virtual double getArea() const = 0;
};
