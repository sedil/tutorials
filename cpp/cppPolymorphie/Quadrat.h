#pragma once

#include "Polygon.h"

class Quadrat : public Polygon {
public:
    explicit Quadrat();
    virtual ~Quadrat();

    std::string getFigurenname() const override;

    double getUmfang(double *args) override;

    void setParameter(const double &p) override;
    double getArea() const override;
};
