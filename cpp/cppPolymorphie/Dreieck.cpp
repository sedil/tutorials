#include "Dreieck.h"

Dreieck::Dreieck() : Polygon(3) { }

Dreieck::~Dreieck() { }

std::string Dreieck::getFigurenname() const {
    return "Dreieck";
}

double Dreieck::getUmfang(double *args) {
    return args[0] + args[1] + args[2];
}

void Dreieck::setParameter(const double &p) {
    area = p;
}

double Dreieck::getArea() const {
    return (area * area / 2);
}
