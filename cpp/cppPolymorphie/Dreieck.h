#pragma once

#include "Polygon.h"

class Dreieck : public Polygon {
public:
    explicit Dreieck();
    virtual ~Dreieck();

    std::string getFigurenname() const override;

    double getUmfang(double *args) override;

    void setParameter(const double &p) override;
    double getArea() const override;
};
