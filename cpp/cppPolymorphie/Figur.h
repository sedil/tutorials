#pragma once

#include <string>

class Figur {
private:
    bool definiert;
protected:
    /* Auf Attribute/Funktionen mit den Zugriffsmodifizierer 'protected' können nur
       Subklassen zugreifen, also Klassen die sich innerhalb der Vererbungshierarchie
       befinden */
    double area;
public:
    explicit Figur(bool d);
    virtual ~Figur();

    /* Wie gewohnt können hier alle Klassen drauf zugreifen, da public */
    bool getDefiniert() const;

    /* virtuelle Funktionen bekommen i.d.R eine Standardimplementierung, die
       von ihren Subklassen überschrieben werden sollen */
    virtual std::string getFigurenname() const;

    /* abstrakte Funktionen müssen in ihren Subklassen implementiert werden. Klassen
       die mindestens eine abstrakte Funktion (pure virtual) haben sind ebenfalls
       Abstrakt. Dies bedeutet, dass keine Instanz dieser Klasse erstellt werden kann */
    virtual void setParameter(const double &p) = 0;
    virtual double getArea() const = 0;
};
