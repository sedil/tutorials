#include "Kreis.h"

/* Wird von einer Klasse geerbt, so muss im Konstruktor der
   Superkonstruktor aufgerufen werden */
Kreis::Kreis() : Figur(true) { }

Kreis::~Kreis() { }

/* Dies ist die sinnvoll ï¿½berschriebene virtuelle Funktion aus
   der Superklasse */
std::string Kreis::getFigurenname() const{
    return "Kreis";
}

/* Dies ist die abstrakte Funktion aus der Superklasse, die hier
   implementiert werden muss */
void Kreis::setParameter(const double &p) {
    /* Man kann auf Attribute der Superklasse zugreifen, wegen der Vererbung */
    area = p;
}

double Kreis::getArea() const {
    return area * area * PI;
}
