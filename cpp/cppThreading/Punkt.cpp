#include "Punkt.h"

Punkt::Punkt(int x, int y){
    _x = x;
    _y = y;
}

Punkt::~Punkt(){}

void Punkt::setX(int x){
    _x = x;
}

void Punkt::setY(int y){
    _y = y;
}

int Punkt::getX() const {
    return _x;
}

int Punkt::getY() const {
    return _y;
}
