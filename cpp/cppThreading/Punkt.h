#ifndef PUNKT_H
#define PUNKT_H

class Punkt {
    int _x,_y;
public:
    explicit Punkt(int x, int y);
    virtual ~Punkt();

    void setX(int x);
    void setY(int y);

    int getX() const;
    int getY() const;
};

#endif // PUNKT_H
