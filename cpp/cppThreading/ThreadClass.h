#include <mutex>

#include "Punkt.h"

class ThreadClass {
private:
    int mutexdemo;
    std::mutex myMutex;

    void uebergaben(Punkt p, Punkt &refP);
    template<typename arrType> long long sumFunction(arrType *feld, int feldsize);
    long long sum(int *feld, int feldsize);
    void synchronisiereFunktion();
public:
    explicit ThreadClass();
    virtual ~ThreadClass();

    void parameterTest();
    int getMaxThreadNumber() const;
    void rechne();
    void synchronisiere();
    void futureBeispiel();
    void packagetaskBeispiel();
};
