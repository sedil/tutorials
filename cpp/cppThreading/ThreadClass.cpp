#include <iostream>
#include <thread>
#include <future>
#include <vector>

#include "ThreadClass.h"

ThreadClass::ThreadClass() : mutexdemo(0) {}

ThreadClass::~ThreadClass() {}

void ThreadClass::uebergaben(Punkt p, Punkt &refP){
    std::cout << "CbV : (" << p.getX() << "," << p.getY() << ")" << "\n";

    refP.setX(refP.getX() + 1);
    std::cout << "CbR : (" << refP.getX() << "," << refP.getY() << ")" << "\n";

    /* Thread-ID innerhalb eines Threads ermitteln */
    std::cout << "Thread-ID "<< std::this_thread::get_id() << "\n";
}

void ThreadClass::parameterTest(){
    Punkt p(1,2);
    Punkt q(3,4);

    /* Parameteruebergabe bei Threads (Funktion, Thread, Parameterliste) */
    std::thread t(&ThreadClass::uebergaben, this, p, std::ref(q));

    /* warten bis aufrufende Funktion verlassen wird */
    t.join();
}

/* Anzahl maximal unterstuetzer Threads fuer das System ermitteln */
int ThreadClass::getMaxThreadNumber() const {
    int tNumber = std::thread::hardware_concurrency();
    if ( tNumber == 0){
        tNumber = 2;
    }
    return tNumber;
}

/* Funktion die jeder Thread einzelnt aufruft */
template <typename arrType>
long long ThreadClass::sumFunction(arrType *feld, int feldsize){
    long long sum = 0;
    for(int i = 0; i < feldsize; i++){
        sum += feld[i];
    }
    return sum;
}

long long ThreadClass::sum(int *feld, int feldsize){
    int maxThreads = getMaxThreadNumber();

    /* Individuell ermitteln */
    const int minDataPerThread = 2000;

    /* maximale Anzahl an Threads die man gerne hÃ¤tte */
    int maxAmountOfThreads = (feldsize + minDataPerThread - 1) / minDataPerThread;

    /* wieviele Threads bekommen wir */
    int amountOfThreads = maxThreads > maxAmountOfThreads ? maxAmountOfThreads : maxThreads;

    /* Daten pro Threads bestimmen */
    int dataPerThread = feldsize / amountOfThreads;

    std::vector<long long> results(amountOfThreads);

    /* -1, da der aufrufende Thread der Funktion schon existiert */
    std::vector<std::thread> threads(amountOfThreads - 1);

    for(int i = 0; i < amountOfThreads; i++){
        threads[i] = std::thread([=](long long &result) {
            result = sumFunction(feld + dataPerThread * i, dataPerThread);
        }, std::ref(results[i]));
    }

    /* aktuell auszufÃ¼hrender Thread */
    int startOffset = dataPerThread * (amountOfThreads - 1);
    results[amountOfThreads - 1] = sumFunction(feld + startOffset, feldsize - startOffset);

    for(int i = 0; i < amountOfThreads - 1; i++){
        threads[i].join();
    }
    return sumFunction(results.data(), results.size());
}

/* Aufrufen, um ein Multithreadingbeispiel zu sehen */
void ThreadClass::rechne(){
    int *feld = new int[10000];
    for(int i = 0; i < 10000; i++){
        feld[i] = i;
    }

    long long ergebnis = sum(feld, 10000);
    std::cout << ergebnis << "\n";
    delete[] feld;
    feld = nullptr;
}

void ThreadClass::synchronisiereFunktion(){
    for(int i = 0; i < 100000; i++){

        /* Zu schÃ¼tzende Variable mit mutex umgeben,
           Ausnahme : const und read-only */
        std::lock_guard<std::mutex> guard(myMutex);
        mutexdemo += 1;
    }
}

/* Aufrufen, um ein Beispiel mit Mutex (synchronisation) zu sehen */
void ThreadClass::synchronisiere(){
    std::vector<std::thread> threads(10);

    for(unsigned int i = 0; i < threads.size(); i++){
        threads[i] = std::thread(&ThreadClass::synchronisiereFunktion, this);
    }

    for(unsigned int i = 0; i < threads.size(); i++){
        threads[i].join();
    }

    std::lock_guard<std::mutex> guard(myMutex);
    std::cout << mutexdemo << "\n";
}

/* Future = eine Aufgabe in ein Thread auslagern
 * Thread = wenn kein Ergebnis benÃ¶tigt wird
 * Future = wenn man ein Ergebnis erwartet */
void ThreadClass::futureBeispiel() {

    /* 1. Testdaten erstellen */
    std::vector<Punkt> punkteliste;
    for(unsigned int i = 0; i < 100; i++){
        punkteliste.push_back(Punkt(1,1));
    }

    /* Optional : Problem in kleinere Teilprobleme unterteilen */
    std::vector<std::vector<Punkt>> teilproblem;
    int sector = 1;
    for(int outer = 0; outer < 100; outer += 10){
        std::vector<Punkt> innerlist;
        for(int inner = 0; inner < 10; inner++){
            innerlist.push_back(punkteliste.at(sector*inner));
        }
        sector++;
        teilproblem.push_back(innerlist);
    }

    /* 2. Erstelle ein Funktionspointer / Lambda */
    auto task = [](std::vector<Punkt> p){
        unsigned int summe = 0;
        for(Punkt elem : p){
            summe += elem.getX();
            summe += elem.getY();
        }
        return summe;
    };

    /* 3. Erstelle ein Future oder Futurelist (tasks) */
    std::vector<std::future<unsigned int>> tasks;
    for(unsigned int i = 0; i < teilproblem.size(); i++){
        //const std::future<unsigned int> future = std::async(task,teilproblem.at(i));
        tasks.push_back(std::async(task,teilproblem.at(i)));
    }

    // Eine normale Aufgabe wird unabhÃ¤ngig vom future ausgefÃ¼hrt
    unsigned int size = punkteliste.size();

    /* 4. Warte auf das future und nehme ergebnis an */
    unsigned int result = 0;
    for(unsigned int i = 0; i < tasks.size(); i++){
        result += tasks.at(i).get();
    }

    float f = static_cast<float>(result/ size);

    std::cout << "Ergebnis : " << f << "\n";
}

/* PackageTask = Genau wie Future, nur man kann selber entscheiden,
 * wann und wo ein Thread/Threadpool ausgefÃ¼hrt werden soll */
void ThreadClass::packagetaskBeispiel(){
    /* 1. Testdaten erstellen */
    int x = 4;
    int y = 6;

    /* 2. Funktionspointer erstellen */
    auto function = [](int x, int y){
        return x + y;
    };

    /* 3. Erstelle ein packagetask und erhalte das NICHT ausgefÃ¼hrte future */
    std::packaged_task<int(int, int)> task(function);
    std::future<int> future = task.get_future();

    /* 4 Hier soll das future selber ausgefÃ¼hrt werden, wann man mÃ¶chte */
    std::thread t(std::move(task), x, y);
    t.join();

    /* 5 Ergebnis von den future erhalten */
    std::cout << future.get();
}
