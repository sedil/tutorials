#include <iostream>
#include <string>
#include <iterator>
#include <algorithm>
#include <vector>
#include <forward_list>
#include <unordered_set>
#include <unordered_map>
#include "Datenstruktur.h"

/* extern : globale Variable von irgendeiner Datei wird (in diesen Fall von Start.cpp)
 * hier fuer irgend ein Zweck (Datentypen::feld) uebergeben. Globale Variablen
 * sind auch von anderen Dateien aufrufbar */
extern short zahl;

void Datenstruktur::auswahl() {
    int auswahl;
    std::cout << "Welche Datenstruktur ? : ";
    std::cin >> auswahl;

    switch (auswahl) {
    case 1: {
        raw_array();
        break;
    }
    case 2 : {
        array();
        break;
    }
    case 3: {
        vector();
        break;
    }
    case 4: {
        list();
        break;
    }
    case 5: {
        set();
        break;
    }
    case 6: {
        map();
        break;
    }
    default:
        break;
    }
}

/* Array ist ein statisches Feld welches mit einer festen groesse initialisiert
werden muss */
void Datenstruktur::raw_array() {

    /* Beispielverwendung fuer 'extern' */
    int arr[zahl];

    /* Beispielverwendung eines typedefs, wie jede andere variable */
    natzahl n = 0;

    for (int i = 0; i < zahl; i++) {
        *(arr + i) = n;
        n++;
    }

    for (int j = 0; j < zahl; j++) {
        std::cout << arr[j] << std::endl;
    }
}

/* Array ist ein statisches Feld welches mit einer festen groesse initialisiert
werden muss, hier aus der Bibliothek std */
void Datenstruktur::array() {
    std::array<int, 4> arr;

    for (unsigned int i = 0; i < arr.size(); i++){
        arr[i] = i+1;
    }

    for (int &elem : arr) {
        std::cout << elem << std::endl;
    }
}

/* Vektor legt eine groesse fest, die sich beim erreichen des maximums beliebig um
neue Elemente erweitern laesst mit push_back(wert) */
void Datenstruktur::vector() {
    std::vector<int> inhalt;

    /* Fuege Elemente hinzu */
    inhalt.push_back(8);
    inhalt.push_back(9);
    inhalt.push_back(5);
    inhalt.push_back(3);

    /* Loesche letztes Element */
    inhalt.pop_back();

    inhalt.insert(inhalt.begin(), 1);

    /* loesche/suche ein Wert im Index */
    int x = 9;
    for (unsigned int i = 0; i < inhalt.size(); i++) {
        if (inhalt.at(i) == x) {
            inhalt.erase(inhalt.begin() + i);
        }
    }

    /* Durchlaufe den Vektor */
    for (unsigned int a = 0; a < inhalt.size(); a++) {
        std::cout << inhalt[a] << std::endl;
    }
}

/* Einfach verkettete Liste mit nur ein Pointer auf sein Nachfolger */
void Datenstruktur::list() {
    std::forward_list<int> fl;
    fl.push_front(5);
    fl.push_front(9);
    fl.push_front(-1);
    fl.push_front(6);

    /* Element in einer Liste finden und entfernen */
    if (std::find(std::begin(fl), std::end(fl), -1) != std::end(fl)) {
        fl.remove(-1);
    }

    /* Durchlaufe die Liste */
    for (auto i = fl.begin(); i != fl.end(); i++) {
        std::cout << *i << std::endl;
    }
}

/* Set laesst keine Duplikate zu */
void Datenstruktur::set() {
    std::unordered_set<int> zahlenset;
    zahlenset.insert(1);
    zahlenset.insert(2);
    zahlenset.insert(1);
    zahlenset.insert(3);
    zahlenset.insert(2);

    /* Iterator durchlaeuft das set */
    for (auto i = zahlenset.begin(); i != zahlenset.end(); i++) {
        std::cout << *i << std::endl;
    }
}

/* Eine Map ordet einem Key einen Value zu */
void Datenstruktur::map() {
    std::unordered_map<int, std::string> zeichenmap;
    zeichenmap.insert(std::pair<int, std::string>(1, "Eins"));
    zeichenmap.insert(std::pair<int, std::string>(2, "Zwei"));
    zeichenmap.insert(std::pair<int, std::string>(3, "Drei"));
    zeichenmap.insert(std::pair<int, std::string>(4, "Vier"));

    int key = zeichenmap.find(2)->first;
    std::string value = zeichenmap.find(2)->second;
    std::cout << key << " : " << value << std::endl;

    zeichenmap.erase(zeichenmap.find(3)->first);

    for (auto it = zeichenmap.begin(); it != zeichenmap.end(); it++) {
        std::cout << it->first << " : " << it->second << std::endl;
    }
}
