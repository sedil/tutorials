#ifndef LAMBDATEST_H
#define LAMBDATEST_H

#include <vector>
#include <functional>

namespace Tutorial {

    class LambdaTest {
    private:
        std::vector<int> testFunction(const std::vector<int>& values, const std::function<std::vector<int>(std::vector<int>)>& func);
    public:
        explicit LambdaTest();
        virtual ~LambdaTest();

        void callTestFunction();
    };

}

#endif // LAMBDATEST_H
