#include "Sort.h"
#include <iostream>

Sort::Sort() { }

Sort::~Sort() { }

void Sort::bubbleSort(){
    const num SIZE = 10;
    num list[SIZE] = {56, 81, 3, 17, 35, 11, 8, 3, 17, 34};
    bool changesDone = true;

    for(num i = 0; i < SIZE && changesDone ; i++){
        changesDone = false;
        for(num j = 0; j < SIZE - 1 - i; j++){
            // Beide Werte vergleichen und tauschen
            // Am Ende steht der groesste Wert ganz oben
            if ( list[j] > list[j+1] ){
                num temp = list[j];
                list[j] = list[j+1];
                list[j+1] = temp;
                changesDone = true;
            }
        }
    }

    for(num res = 0; res < SIZE; res++){
        std::cout << list[res] << "\n";
    }
}

void Sort::insertionSort(){
    const num SIZE = 10;
    num list[SIZE] = {56, 81, 3, 17, 35, 11, 8, 3, 17, 34};

    for(num i = 0; i < SIZE; i++){
        num valToSort = list[i];
        num k = i;

        while( k > 0 && list[k-1] > valToSort ){
            list[k] = list[k-1];
            k--;
        }
        list[k] = valToSort;
    }

    for(num res = 0; res < SIZE; res++){
        std::cout << list[res] << "\n";
    }

}

void Sort::quickSort(num *arr, num links, num rechts){
    num i = links;
    num j = rechts;
    num temp;
    num pivot = arr[((rechts + links) / 2)];

    while( i <= j ){
        while( arr[i] < pivot ){
            i++;
        }

        while( arr[j] > pivot ){
            j--;
        }

        if( i <= j ){
            temp = arr[i];
            arr[i] = arr[j];
            arr[j] = temp;
            i++;
            j--;
        }
    }

    if( links < j){
        quickSort(arr, links, j);
    }
    if( i < rechts ){
        quickSort(arr, i, rechts);
    }
}

void Sort::merge(num arr[], num l, num m, num r) {
    num i, j, k;
    num n1 = m - l + 1;
    num n2 =  r - m;

    /* create temp arrays */
    num L[n1], R[n2];

    /* Copy data to temp arrays L[] and R[] */
    for (i = 0; i < n1; i++){
        L[i] = arr[l + i];
    }

    for (j = 0; j < n2; j++){
        R[j] = arr[m + 1+ j];
    }

    /* Merge the temp arrays back into arr[l..r]*/
    i = 0; // Initial index of first subarray
    j = 0; // Initial index of second subarray
    k = l; // Initial index of merged subarray
    while (i < n1 && j < n2) {
        if (L[i] <= R[j]) {
            arr[k] = L[i];
            i++;
        } else {
            arr[k] = R[j];
            j++;
        }
        k++;
    }

    /* Copy the remaining elements of L[], if there
       are any */
    while (i < n1) {
        arr[k] = L[i];
        i++;
        k++;
    }

    /* Copy the remaining elements of R[], if there
       are any */
    while (j < n2) {
        arr[k] = R[j];
        j++;
        k++;
    }
    for(num res = 0; res < 10; res++){
        std::cout << arr[res] << "\n";
    }
}

/* l is for left index and r is right index of the
   sub-array of arr to be sorted */
void Sort::mergeSort(num arr[], num l, num r) {
    if (l < r) {
        // Same as (l+r)/2, but avoids overflow for
        // large l and h
        num m = l+(r-l)/2;

        // Sort first and second halves
        mergeSort(arr, l, m);
        mergeSort(arr, m+1, r);

        merge(arr, l, m, r);
    }
}

