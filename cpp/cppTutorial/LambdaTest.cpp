#include <iostream>

#include "LambdaTest.h"

using namespace Tutorial;

LambdaTest::LambdaTest() { }

LambdaTest::~LambdaTest() { }

/* (3) = define a functionpointer as argument: void(int) means = returntype is void, parameter is int */
std::vector<int> LambdaTest::testFunction(const std::vector<int>& values, const std::function<std::vector<int>(std::vector<int>)>& func) {
    /* (4) = execute the lambda */
    std::vector<int> result = func(values);
    return result;
}

void LambdaTest::callTestFunction() {
    // fill vector with integers
    std::vector<int> vec = std::vector<int>();
    for(int i = 0; i < 25; i++){
        vec.push_back(i);
    }

    /* (1) = define a lambda expression: auto lambda = [capture](arguments){code}
     * [=] = capture all arguments as const
     * [&] = capture all arguments as reference
     * [=,&a] = capture all arguments as const execpt for a as reference */
    // variable for capture test
    short div = 2;

    auto lambda = [&div](std::vector<int> vector){
        std::vector<int> equalValues = std::vector<int>();
        for(int elem : vector){
            if(elem % div == 0){
                equalValues.push_back(elem);
            }
        }
        return equalValues;
    };

    /* (2) = use the lambda. In this case pass lambda as parameter to a function */
    std::vector<int> result = testFunction(vec, lambda);

    for(int elem : result){
        std::cout << elem << "\n";
    }

}
