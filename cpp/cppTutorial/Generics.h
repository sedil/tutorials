#include <string>

/*	Bei Templates muss die Implementierung der Funktionen in der
	Headerdatei stattfinden und nicht in der .cpp-Datei. Es wird
	keine .cpp-Datei benoetigt

	wird kein Parameter angegeben, dann lautet der Datentyp int */
template<typename T = int> class Punkt {
private:
	T _x, _y;
public:
	explicit Punkt(T, T);
	virtual ~Punkt();
	void setX(T);
	void setY(T);
	T getX() const;
	T getY() const;
	std::string toString() const;
};

template<typename T>
Punkt<T>::Punkt(T x, T y) :
	_x(x),
	_y(y)
	{}

template<typename T>
Punkt<T>::~Punkt() {}

template<typename T>
void Punkt<T>::setX(T x) {
	_x = x;
}

template<typename T>
void Punkt<T>::setY(T y) {
	_y = y;
}

template<typename T>
T Punkt<T>::getX() const {
	return _x;
}

template<typename T>
T Punkt<T>::getY() const {
	return _y;
}

/* Template spezialisierung fuer den Fall : long */
template<>
long Punkt<long>::getY() const {
    return -1;
}

template<typename T>
std::string Punkt<T>::toString() const {
	return "(" + std::to_string(_x) + "," + std::to_string(_y) + ")";
}
