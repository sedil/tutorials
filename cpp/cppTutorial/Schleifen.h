class Schleifen {
public:
	void schalter(int);
private:
	void drucke(int const&);
	int forSchleife(int);
    int forEachSchleife();
    int forAutoSchleife();
	int whileSchleife();
	int doSchleife(int);
	int rekursion(int);
};
