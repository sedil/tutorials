class Call {
public:
	void callX();
private:
	void callByValue(short);
	void callByReference(short&);
	void callByReferenceConst(const short&);
	short* pointerUebergabe(short*);
    short* pointerRueckgabe();
	void pointerUebergabeVoid(short*);
	long& komplexDemo(unsigned short, const int&, long&, int*, int);
};
