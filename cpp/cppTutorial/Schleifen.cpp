#include <iostream>
#include "Schleifen.h"

void Schleifen::drucke(int const &wert) {
    std::cout << wert << std::endl;
}

void Schleifen::schalter(int eingabe) {
    const int COUNTER = 10;
    int ergebnis;

    if (eingabe >= 1 && eingabe <= 6) {
        switch (eingabe) {
        case 1: ergebnis = forSchleife(COUNTER);
            drucke(ergebnis);
            break;
        case 2: ergebnis = forEachSchleife();
            drucke(ergebnis);
            break;
        case 3: ergebnis = forAutoSchleife();
            drucke(ergebnis);
            break;
        case 4: ergebnis = whileSchleife();
            drucke(ergebnis);
            break;
        case 5: ergebnis = doSchleife(COUNTER);
            drucke(ergebnis);
            break;
        case 6: ergebnis = rekursion(5);
            drucke(ergebnis);
        default:
            break;
        }
    }
    else {
        std::cout << "UngÃ¼ltiger Wert" << std::endl;
    }
}

int Schleifen::forSchleife(const int wert) {
    int rechnung = 0;
    for (int i = 0; i < wert; i++) {
        rechnung += 5;
    }
    return rechnung;
}

int Schleifen::forEachSchleife() {
    int rechnung = 0;

    int feld[3];
    feld[0] = 1;
    feld[1] = 2;
    feld[2] = 3;

    for (int zahl : feld) {
        rechnung += zahl;
    }
    return rechnung;
}

int Schleifen::forAutoSchleife() {
    int rechnung = 0;

    int feld[3];
    feld[0] = 1;
    feld[1] = 2;
    feld[2] = 3;

    for (auto zahl : feld) {
        rechnung += zahl;
    }
    return rechnung;
}

int Schleifen::whileSchleife() {
    int wert = 10;
    while (wert > 0) {
        wert--;
    }
    return -1;
}

int Schleifen::doSchleife(const int wert) {
    int rechnung = wert;
    int z = 10;
    do {
        rechnung += wert;
        z--;
    } while (z > 0);
    return rechnung;
}

int Schleifen::rekursion(int wert) {
    if (wert == 0) {
        return 1;
    }
    else {
        return wert * rekursion(wert - 1);
    }
}
