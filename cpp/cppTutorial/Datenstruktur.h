#include <iostream>

/* "typedef" : Eigene Datentypen können definiert werden, oder bestehende umbenannt werden */
typedef unsigned short natzahl;

struct Datenstruktur {
	void auswahl();
    void raw_array();
    void array();
    void vector();
    void list();
	void set();
	void map();
};
