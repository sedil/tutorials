#include <iostream>
#include <vector>
#include <string>

class Stock {
private:
    std::vector<class Observer*> _observerlist;
    unsigned int _stockValue;
public:
    explicit Stock(){
        _stockValue = 0;
    }

    virtual ~Stock(){}

    void registerObserver(Observer *observer){
        _observerlist.push_back(observer);
    }

    void setValue(unsigned int value){
        _stockValue = value;
        notify();
    }

    unsigned int getStockValue() const {
        return _stockValue;
    }

    void notify();
};

class Observer {
private:
    Stock *_stock;
public:
    explicit Observer(Stock *stock){
        _stock = stock;
        _stock->registerObserver(this);
    }

    virtual void update() = 0;
protected:
    Stock* getStock(){
        return _stock;
    }
};

void Stock::notify(){
    for(unsigned int i = 0; i < _observerlist.size(); i++){
        _observerlist.at(i)->update();
    }
}

class Steem : public Observer {
public:
    explicit Steem(Stock *steemStock) : Observer(steemStock) { }

    void update(){
        std::cout << "STEEM " << getStock()->getStockValue() << "\n";
    }
};

class BitCoin : public Observer {
public:
    explicit BitCoin(Stock *btcStock) : Observer(btcStock) { }

    void update(){
        std::cout << "BITCOIN " << getStock()->getStockValue() << "\n";
    }
};

class SBD : public Observer {
public:
    explicit SBD(Stock *sbdStock) : Observer(sbdStock) { }

    void update(){
        std::cout << "SBD " << getStock()->getStockValue() << "\n";
    }
};

class ObserverTest {
public:
    explicit ObserverTest(){
        Stock stock;
        Steem steem(&stock);
        BitCoin bitcoin(&stock);
        SBD sbd(&stock);
        stock.setValue(5);
    }
};
