#include <iostream>
#include <string>

class Computer {
public:
    virtual void boot() = 0;
    virtual void shutdown() = 0;
    virtual std::string getStatus() = 0;
    virtual std::string getType() = 0;

    virtual ~Computer(){}
};

class Desktop : public Computer {
private:
    bool _status;
public:
    explicit Desktop(){
        _status = false;
    }

    virtual ~Desktop() {}

    void boot() override {
        _status = true;
    }

    void shutdown() override {
        _status = false;
    }

    std::string getStatus() override {
        if( _status == true ){
            return "On";
        } else {
            return "Off";
        }
    }

    std::string getType() override {
        return "Desktop";
    }
};

class Laptop : public Computer {
private:
    bool _status;
public:
    explicit Laptop(){
        _status = false;
    }

    virtual ~Laptop() {}

    void boot() override {
        _status = true;
    }

    void shutdown() override {
        _status = false;
    }

    std::string getStatus() override {
        if( _status == true ){
            return "On";
        } else {
            return "Off";
        }
    }

    std::string getType() override {
        return "Laptop";
    }
};

class ComputerFactory {
public:
    explicit ComputerFactory(){}
    virtual ~ComputerFactory(){}

    Computer* createComputer(const std::string &type){
        if( type == "desktop"){
            return new Desktop;
        } else if ( type == "laptop"){
            return new Laptop;
        }
        return nullptr;
    }
};
